% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
% Defines some basic physical constants.
classdef PhysConst
    properties (Constant)
        AMU = 1e-3./PhysConst.N_A;          %Atomic mass unit [kg]
        k_B = 1.3806503e-23;                %Boltzmann constant [J*K^-1]
        e = 1.6022e-19;                     %Elementary electron [Coulombs]
        qe= -PhysConst.e                    %Charge electron [Coulombs]
        eps0 = 8.854187e-12;                %Vacuum permittivity [F/m][A^2*s^4*kg^−1*m^−3]
        m_e  = 9.10938356e-31;              %Mass of electron [kg]
        h =6.62606877e-34;                  %Planck constant [J*s]
        hbar=1.0545718e-34                  %Reduced Planck constant [J*s]
        eV = PhysConst.e;                   %Electron volt [J]
        c=299792458;                        %Speed of light [m/s]
        alpha=0.0072973525664               %Fine structure constant [] =1/137.035999139
        IpH=13.59844.*PhysConst.eV;         %Ionization potential Hydrogen [J]
        IpHe=24.587387936.*PhysConst.eV;    %Ionization potential Helium [J]
        IpNe=21.5645.*PhysConst.eV;         %Ionization potential Neon [J]
        IpAr=15.7596112.*PhysConst.eV;      %Ionization potential Argon [J]
        IpKr=13.9996049.*PhysConst.eV;      %Ionization potential Krypton [J]
        IpXe=12.1298431.*PhysConst.eV;      %Ionization potential Xenon [J]
        HartreeToInvCm=219474.63068;        %multiply to go from Hartree energy units to cm^{-1}
        EHartree=27.21138602.*PhysConst.eV; %Hartree energy [J]
        ElectricFieldAtomic=5.14220652e11   %1 a.u.=5.14220652e11 [V/m]
        a0=5.2917721092e-11;                %Bohr radius [m]
        eVT = PhysConst.eV./PhysConst.k_B;  %Temperature eV [K]
        N_A = 6.02214076e23;                %Avogadro constant (2019-exact) []
        R=PhysConst.k_B.*PhysConst.N_A;     %Gas constant [J/(K*mol)]
        g=9.81;                             %Acceleration due to gravity at sea level [ms^-2]
        r_e=PhysConst.alpha.^2.*PhysConst.a0; %Classical electron radius
        p_atm=1.01325e5;                    %atmospheric pressure [Pa]
        cal=4.184;                          %Thermochemical calorie [J]
        euler_gamma=0.57721566490153286061  %Euler Mascheroni constant
        
        %Element masses [kg]
        m_Al = 26.9815386.*PhysConst.AMU;
        m_Ar=39.948.*PhysConst.AMU;
        m_C=12.011*PhysConst.AMU;
        m_Ce=140.12*PhysConst.AMU;
        m_Cl=35.45*PhysConst.AMU;
        m_Fe=55.845*PhysConst.AMU;
        m_H=1.00794*PhysConst.AMU;
        m_He=4.0026*PhysConst.AMU;
        m_Hg=200.59*PhysConst.AMU;
        m_I=126.90047*PhysConst.AMU;
        m_In=(0.0429*112.904058+0.9571*114.903878)*PhysConst.AMU;
        m_Li=6.94*PhysConst.AMU;
        m_Mn=54.938045*PhysConst.AMU;
        m_N=14.007*PhysConst.AMU;
        m_Na=22.98977*PhysConst.AMU;
        m_Ne=20.17.*PhysConst.AMU;
        m_O=15.999*PhysConst.AMU;
        m_Pb=207.2*PhysConst.AMU;
        m_S=32.06*PhysConst.AMU;
        m_Si=28.085*PhysConst.AMU;
        m_Sn=118.69*PhysConst.AMU;
        m_Tl=(0.2952*202.9723442+0.7048*204.9744275)*PhysConst.AMU;
        m_Xe=131.3*PhysConst.AMU;
    end
end
