Supplementary Matlab code used to obtain the results present in
Multicomponent transport in plasmas; exploiting stoichiometry.

main.m is the entry point for this code. Numerical experiments were conducted in Matlab 2019a.

To run a single input file:
run_folders=false
input_filename=name of input file (without.m)

To run every input file in the "inputs" directory:
run_folders=true
directories={'cell','of','folder','names'}
It is possible to run all input files in parallel by using parfor,
however this may lead to inaccurate timings.

Output is provided in input_struct, which is serialized to a .json
file.