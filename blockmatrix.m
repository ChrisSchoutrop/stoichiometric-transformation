% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef blockmatrix < handle
    %{
    Ax=b where A is a sparse blockmatrix. This class also handles the
    assembly of the system matrix.
    %}
    %{
    This assembly assumes a grid of the form:
    |--0--|--1--|--2--|--3--|--4--|--5--|
    Here the nodes are located at x0,x1,x2,x3,x4,x5.
    The physical grid runs from x_west=(x0+x1)/2 to x_east=(x4+x5)/2. To enforce the
    Dirichlet boundary conditions at x_west and x_east, the nodes x0 and x5 have fixed values.
    %}
    properties
        %{
        Essential properties
        %}
        A       %System matrix
        b       %Right-hand side corresponding to the system matrix
        x=[]    %Solution to Ax=b
        
        %{
        Diagnostic info
        %}
        solve_time=0    %Time spent solving linear the system
        cond_scaled     %Condition number of A/J after row/column scaling
        cond_unscaled   %Condition number of A/J before row/column scaling
        residual        %b-Ax
        
        %{
        Variables relevant to Newton iterations
        %}
        r
        J
    end
    
    methods(Access=public)
        function obj=blockmatrix()
        end
        function r=determine_relative_residual(obj,x)
            r=norm(obj.b-obj.A*x,2)./norm(obj.b,2);
            return
        end
        function r=determine_residual(obj,x)
            r=norm(obj.b-obj.A*x,2);
            return
        end
        function obj=solve_with_guess_Newton_reduced(obj,guess,known_phi)
            %{
            Compute the update from the residual and the Jacobian
            of the residual.
            Preconditions:
            known_phi is the length of number of species
            guess contains the exactly known values of phi exactly, other values are NaN
            %}
            method=2;
            start_time=posixtime(datetime('now'));
            m=length(known_phi);
            N=length(obj.J)/m;      %Number of gridpoints
            
            %{
            We want to solve:
            obj.x=obj.J\obj.r;

            method 1: reduce the system using knowledge of exactly known
            transformed quantities. Then solve directly using mldivide
            
            method 2: Same as method 1, but also scales with absolute row
            and column sums before solving with mldivide
            
            method 3: Try to solve the scaled system with GMRES and ILUT
            preconditioner without fill
            %}
            warning('off','MATLAB:gmres:tooSmallTolerance')
            switch method
                case 1
                    %Mask to filter out the known phi values
                    filter_unknown_phi=repmat(double(isnan(known_phi)),N,1);
                    
                    %Use that the residual and update must be 0 for the
                    %known phi-values to reduce the system we have to
                    %solve.
                    small_J=sparse(obj.J(filter_unknown_phi==1,filter_unknown_phi==1));
                    small_r=obj.r(filter_unknown_phi==1);
                    
                    %Solve the reduced system
                    small_x=small_J\small_r;
                    
                    %Fill in the computed values
                    obj.x=zeros(size(guess));
                    obj.x(filter_unknown_phi==1)=small_x;
                case 2
                    %Mask to filter out the known phi values
                    filter_unknown_phi=repmat(double(isnan(known_phi)),N,1);
                    
                    %Use that the residual and update must be 0 for the
                    %known phi-values to reduce the system we have to
                    %solve.
                    small_J=sparse(obj.J(filter_unknown_phi==1,filter_unknown_phi==1));
                    small_r=obj.r(filter_unknown_phi==1);
                    
                    %Scale with the absolute row and column sums, this strongly
                    %reduces the condition number
                    Q=spdiags(1./sum(abs(small_J),2),0,length(small_J),length(small_J));
                    P=spdiags(1./sum(abs(Q*small_J),1)',0,length(small_J),length(small_J));
                    
                    %Solve the reduced, scaled system
                    small_x=P*((Q*small_J*P)\(Q*small_r));
                    
                    %Fill in the computed values
                    obj.x=zeros(size(guess));
                    obj.x(filter_unknown_phi==1)=small_x;
                case 3
                    %Mask to filter out the known phi values
                    filter_unknown_phi=repmat(double(isnan(known_phi)),N,1);
                    
                    %Use that the residual and update must be 0 for the
                    %known phi-values to reduce the system we have to
                    %solve.
                    small_J=sparse(obj.J(filter_unknown_phi==1,filter_unknown_phi==1));
                    small_r=obj.r(filter_unknown_phi==1);
                    
                    %Scale with the absolute row and column sums, this strongly
                    %reduces the condition number
                    Q=spdiags(1./sum(abs(small_J),2),0,length(small_J),length(small_J));
                    P=spdiags(1./sum(abs(Q*small_J),1)',0,length(small_J),length(small_J));
                    
                    %Solve the reduced, scaled system
                    [L,U] = ilu(Q*small_J*P,struct('type','nofill'));
                    %[small_x,flag,relres,iter]=gmres(Q*small_J*P,Q*small_r,[],1e-12,min([2000,length(small_J)]),[],[],P\guess(filter_unknown_phi==1));
                    %[small_x,flag,relres,iter]=bicgstab(Q*small_J*P,Q*small_r,1e-12,min([2000,length(small_J)]),[],[],P\guess(filter_unknown_phi==1));
                    [small_x,flag,relres,iter]=gmres(Q*small_J*P,Q*small_r,[],1e-12,min([2000,length(small_J)]),L,U,P\guess(filter_unknown_phi==1));
                    %[small_x,flag,relres,iter]=bicgstab(Q*small_J*P,Q*small_r,1e-12,min([2000,length(small_J)]),L,U,P\guess(filter_unknown_phi==1));
                    small_x=P*small_x;
                    disp(['Flag: ',num2str(flag),' relres: ',num2str(relres),' iter: ',num2str(iter)])
                    %small_x=P*((Q*small_J*P)\(Q*small_r));
                    
                    %Fill in the computed values
                    obj.x=zeros(size(guess));
                    obj.x(filter_unknown_phi==1)=small_x;
                otherwise
                    small_J=obj.J;
                    obj.x=obj.J\obj.r;
            end
            obj.solve_time=posixtime(datetime('now'))-start_time;
            
            %Compute and save diagnostic info
            if exist('Q','var')
                obj.cond_scaled=condest(Q*small_J*P);
                disp(['Condition number scaled matrix:' ,num2str(obj.cond_scaled,6)])
            else
                obj.cond_scaled=NaN;
            end
            obj.cond_unscaled=condest(small_J);
            obj.residual=norm(obj.r-obj.J*obj.x)./(norm(obj.r));
            disp(['Condition number discretization matrix:' ,num2str(obj.cond_unscaled,6)])
            disp(['Residual b-Ax:',num2str(obj.residual,6)])
            disp(['Time to solve linear system:',num2str(obj.solve_time),' seconds'])
        end
        function obj=solve_with_guess_Newton(obj,guess)
            %{
            Compute the update from the residual and the (approximate) Jacobian
            of the residual.
            %}
            tic;
            method=1;
            
            N=length(obj.A);
            start_time=posixtime(datetime('now'));
            %{
            Scaling the discretization matrix A with the inverse row-sums
            massively reduces the condition number of A.
            %}
            switch method
                case 1
                    Q=spdiags(1./sum(abs(obj.J),2),0,N,N);
                    P=spdiags(1./sum(abs(Q*obj.J),1)',0,N,N);
                    
                    obj.x=P*((Q*obj.J*P)\(Q*obj.r));
                case 2
                    obj.x=obj.J\obj.r;
                case 3
                    maxit=2000;
                    if maxit>N
                        maxit=N;
                    end
                    [L,U] = ilu(obj.J,struct('type','nofill'));
                    warning off 'MATLAB:gmres:tooSmallTolerance'
                    [obj.x,~,~,iter]=gmres(obj.J,obj.r,[],1e-12,maxit,L,U,guess);
                    disp(['#Iterations: ',num2str(iter)])
                case 4
                    maxit=2000;
                    if maxit>N
                        maxit=N;
                    end
                    QAP=obj.J;
                    Q=spdiags(1./sum(abs(QAP),2),0,N,N);
                    QAP=Q*QAP;
                    P=spdiags(1./sum(abs(QAP),1)',0,N,N);
                    QAP=QAP*P;
                    [L,U] = ilu(QAP,struct('type','nofill'));
                    warning off 'MATLAB:gmres:tooSmallTolerance'
                    %[L,U] = ilu(Q*obj.A*P,struct('type','ilutp'));
                    %[obj.x,flag,relres,iter]=gmres(Q*obj.A*P,Q*obj.b,[],1e-12,maxit,L,U,P\guess);
                    [obj.x,~,~,iter]=bicgstab(QAP,Q*obj.r,1e-12,maxit,L,U,P\guess);
                    %[obj.x,flag,relres,iter]=bicgstab(Q*obj.A*P,Q*obj.b,1e-12,maxit,[],[],P\guess);
                    obj.x=P*obj.x;
                    disp(['#Iterations: ',num2str(iter)])
            end
            obj.solve_time=posixtime(datetime('now'))-start_time;
            
            if exist('Q','var')
                obj.cond_scaled=condest(Q*obj.J*P);
                disp(['Condition number scaled matrix:' ,num2str(obj.cond_scaled,6)])
            else
                obj.cond_scaled=NaN;
            end
            
            obj.cond_scaled=condest(Q*obj.J*P);
            obj.cond_unscaled=condest(obj.J);
            obj.residual=norm(obj.r-obj.J*obj.x)./(norm(obj.r));
            disp(['Condition number discretization matrix:' ,num2str(obj.cond_unscaled,6)])
            disp(['Residual b-Ax:',num2str(obj.residual,6)])
            disp(['Time to solve linear system:',num2str(obj.solve_time),' seconds'])
        end
        function obj=solve_with_guess_Picard(obj,guess)
            %{
            Solves the system using constructed based on Picard iteration.
            %}
            tic;
            method=1;
            warning off 'MATLAB:gmres:tooSmallTolerance'
            
            N=length(obj.A);
            
            %Start the timer
            t=posixtime(datetime('now'));
            %{
            Scaling the discretization matrix A with the inverse row-sums
            massively reduces the condition number of A.
            %}
            switch method
                case 1
                    Q=spdiags(1./sum(abs(obj.A),2),0,N,N);
                    P=spdiags(1./sum(abs(Q*obj.A),1)',0,N,N);
                    
                    obj.x=P*((Q*obj.A*P)\(Q*obj.b));
                case 2
                    obj.x=obj.A\obj.b;
                case 3
                    maxit=min([2000,length(obj.A)]);
                    [L,U] = ilu(obj.A,struct('type','nofill'));
                    [obj.x,flag,relres,iter]=gmres(obj.A,obj.b,[],1e-12,maxit,L,U,guess);
                    disp(['#Iterations: ',num2str(iter)])
                case 4
                    maxit=min([2000,length(obj.A)]);
                    QAP=obj.A;
                    Q=spdiags(1./sum(abs(QAP),2),0,N,N);
                    QAP=Q*QAP;
                    P=spdiags(1./sum(abs(QAP),1)',0,N,N);
                    QAP=QAP*P;
                    [L,U] = ilu(QAP,struct('type','nofill'));
                    %[L,U] = ilu(Q*obj.A*P,struct('type','ilutp'));
                    [obj.x,flag,relres,iter]=gmres(Q*obj.A*P,Q*obj.b,[],1e-12,maxit,L,U,P\guess);
                    %[obj.x,flag,relres,iter]=bicgstab(QAP,Q*obj.b,1e-12,maxit,L,U,P\guess);
                    %[obj.x,flag,relres,iter]=bicgstab(Q*obj.A*P,Q*obj.b,1e-12,maxit,[],[],P\guess);
                    obj.x=P*obj.x;
                    disp(['#Iterations: ',num2str(iter)])
            end
            
            %Stop the timer, we only want time to solve the linear system,
            %not all diagnostic info such as computing condition numbers.
            obj.solve_time=posixtime(datetime('now'))-t;
            if exist('Q','var')
                obj.cond_scaled=condest(Q*obj.A*P);
                disp(['Condition number scaled matrix:' ,num2str(obj.cond_scaled,6)])
            else
                obj.cond_scaled=NaN;
            end
            obj.cond_unscaled=condest(obj.A);
            obj.cond_scaled=condest(Q*obj.A*P);
            obj.residual=norm(obj.b-obj.A*obj.x)./(norm(obj.b));
            disp(['Condition number discretization matrix:' ,num2str(obj.cond_unscaled,6)])
            disp(['Residual b-Ax:',num2str(obj.residual,6)])
            disp(['Time to solve linear system:',num2str(obj.solve_time),' seconds'])
        end
        function obj=solve_with_guess_Picard_reduced(obj,guess,known_phi)
            %{
            Solves the system using constructed based on Picard iteration.
            Here the system is reduced to a smaller system, if certain
            invariants are known (e.g. conservation of mass and
            quasi-neutrality).
            %}            
            tic;
            method=1;
            warning off 'MATLAB:gmres:tooSmallTolerance'
            
            N=length(obj.A);
            
            %Start the timer
            t=posixtime(datetime('now'));
            %{
            Scaling the discretization matrix A with the inverse row-sums
            massively reduces the condition number of A.
            %}
            switch method
                case 1
                    m=length(known_phi);
                    N=length(obj.A)/m;      %Number of gridpoints
                    %Mask to filter out the known phi values
                    filter_unknown_phi=repmat(double(isnan(known_phi)),N,1);
                    
                    %Use that the residual and update must be 0 for the
                    %known phi-values to reduce the system we have to
                    %solve.
                    phi_known=zeros(size(guess));   %ok
                    phi_known(filter_unknown_phi==0)=guess(filter_unknown_phi==0);  %ok
                    b_add=-obj.A*(phi_known);
                    
                    small_A=sparse(obj.A(filter_unknown_phi==1,filter_unknown_phi==1));
                    small_b=obj.b(filter_unknown_phi==1)+b_add(filter_unknown_phi==1);
                    
                    %Solve the reduced system
                    Q=spdiags(1./sum(abs(small_A),2),0,length(small_A),length(small_A));
                    P=spdiags(1./sum(abs(Q*small_A),1)',0,length(small_A),length(small_A));
                    
                    small_x=P*((Q*small_A*P)\(Q*small_b));
                    
                    %small_x=small_A\small_b;
                    
                    %Fill in the computed values
                    obj.x=zeros(size(guess));
                    obj.x(filter_unknown_phi==1)=small_x;
                    obj.x(filter_unknown_phi==0)=guess(filter_unknown_phi==0);
                case 2
                    m=length(known_phi);
                    N=length(obj.A)/m;      %Number of gridpoints
                    %Mask to filter out the known phi values
                    filter_unknown_phi=repmat(double(isnan(known_phi)),N,1);
                    
                    %Use that the residual and update must be 0 for the
                    %known phi-values to reduce the system we have to
                    %solve.
                    phi_known=zeros(size(guess));   %ok
                    phi_known(filter_unknown_phi==0)=guess(filter_unknown_phi==0);  %ok
                    b_add=-obj.A*(phi_known);
                    
                    small_A=sparse(obj.A(filter_unknown_phi==1,filter_unknown_phi==1));
                    small_b=obj.b(filter_unknown_phi==1)+b_add(filter_unknown_phi==1);
                    
                    %Solve the reduced system
                    Q=spdiags(1./sum(abs(small_A),2),0,length(small_A),length(small_A));
                    P=spdiags(1./sum(abs(Q*small_A),1)',0,length(small_A),length(small_A));
                    maxit=min([2000,length(small_A)]);
                    [L,U] = ilu(Q*small_A*P,struct('type','nofill'));
                    small_x=gmres(Q*small_A*P,Q*small_b,[],1e-12,maxit,L,U,P\guess(filter_unknown_phi==1));
                    small_x=P*small_x;
                    %small_x=small_A\small_b;
                    
                    %Fill in the computed values
                    obj.x=zeros(size(guess));
                    obj.x(filter_unknown_phi==1)=small_x;
                    obj.x(filter_unknown_phi==0)=guess(filter_unknown_phi==0);
            end
            
            %Stop the timer, we only want time to solve the linear system,
            %not all diagnostic info such as computing condition numbers.            
            obj.solve_time=(posixtime(datetime('now'))-t);
            if exist('Q','var')
                obj.cond_scaled=condest(Q*small_A*P);
                disp(['Condition number scaled matrix:' ,num2str(obj.cond_scaled,6)])
            else
                obj.cond_scaled=NaN;
            end
            obj.cond_unscaled=condest(small_A);
            obj.cond_scaled=condest(Q*small_A*P);
            obj.residual=norm(obj.b-obj.A*obj.x)./(norm(obj.b));
            disp(['Condition number discretization matrix:' ,num2str(obj.cond_unscaled,6)])
            disp(['Residual b-Ax:',num2str(obj.residual,6)])
            disp(['Time to solve linear system:',num2str(obj.solve_time),' seconds'])
        end
        function obj=assemble_phi(obj,grid_old,grid_new,theta,dt,isNewton)
            %{
            Setup discretization matrix for element part of phi vector
            (last N_elements components)
            This yields N_elements equations per gridpoint
            Elements are assumed to be bottom part of phi vector
            Assuming Dirichlet BC
            
            _old indicates the previous timestep
            _new indicates an estimate for the next timestep
            %}
            dx=grid_old.dx;
            
            N=grid_old.N;   %Number of gridpoints
            m=grid_old.m;   %Number of species
            
            I  = eye(m);
            s_old=grid_old.get_transformed_source();
            s_new=grid_new.get_transformed_source();
            
            phi_old=grid_old.get_transformed_phi();
            phi_new=grid_new.get_transformed_phi();
            
            %<construct_A_old>
            Amd =zeros(m,m,N);
            Asub=zeros(m,m,N-1);
            Asup=zeros(m,m,N-1);
            
            %Loop over grid
            for iter=2:N-1
                %Assemble diagonal block
                mat = (I+grid_old.v_delta{iter-1}-grid_old.v_gamma{iter});
                %mat=ones(m,m)*(iter);
                Amd(:,:,iter) = mat;
                
                %Assemble sub diagonal block
                mat=+grid_old.v_gamma{iter-1};
                Asub(:,:,iter-1)=mat;
                
                %Assemble super diagonal block
                mat=-grid_old.v_delta{iter};
                Asup(:,:,iter)=mat;
            end
            
            %Final assembly
            A_old = blktridiag(Amd,Asub,Asup);
            %</construct_A_old>
            
            %<construct_A_new>
            Amd =zeros(m,m,N);
            Asub=zeros(m,m,N-1);
            Asup=zeros(m,m,N-1);
            
            %Loop over grid
            for iter=2:N-1
                %Assemble diagonal block
                mat = (I+grid_new.v_delta{iter-1}-grid_new.v_gamma{iter});
                Amd(:,:,iter) = mat;
                
                %Assemble sub diagonal block
                mat=+grid_new.v_gamma{iter-1};
                Asub(:,:,iter-1)=mat;
                
                %Assemble super diagonal block
                mat=-grid_new.v_delta{iter};
                Asup(:,:,iter)=mat;
            end
            
            %Final assembly
            A_new = blktridiag(Amd,Asub,Asup);
            %</construct_A_new>
            
            %<construct_B_old>
            Amd =zeros(m,m,N);
            Asub=zeros(m,m,N-1);
            Asup=zeros(m,m,N-1);
            
            %Loop over grid
            for iter=2:N-1
                %Assemble diagonal block
                mat =-grid_old.v_beta{iter-1}-grid_old.v_alpha{iter};
                Amd(:,:,iter) = mat;
                
                %Assemble sub diagonal block
                mat=+grid_old.v_alpha{iter-1};
                Asub(:,:,iter-1)=mat;
                
                %Assemble super diagonal block
                mat=+grid_old.v_beta{iter};
                Asup(:,:,iter)=mat;
            end
            
            %Final assembly
            B_old = blktridiag(Amd,Asub,Asup);
            %</construct_B_old>
            
            %<construct_B_new>
            Amd =zeros(m,m,N);
            Asub=zeros(m,m,N-1);
            Asup=zeros(m,m,N-1);
            
            %Loop over grid
            for iter=2:N-1
                %Assemble diagonal block
                mat = -grid_new.v_alpha{iter}-grid_new.v_beta{iter-1};
                Amd(:,:,iter) = mat;
                
                %Assemble sub diagonal block
                mat=+grid_new.v_alpha{iter-1};
                Asub(:,:,iter-1)=mat;
                
                %Assemble super diagonal block
                mat=+grid_new.v_beta{iter};
                Asup(:,:,iter)=mat;
            end
            %Final assembly
            B_new = blktridiag(Amd,Asub,Asup);
            %</construct_B_new>
            
            %{
            Complete the scheme
            %}
            if isinf(dt)==false
                obj.A=theta*A_new+(1-theta)*A_old-theta*B_new*dt./dx;
                obj.b=(theta*A_new+(1-theta)*A_old+(1-theta)*B_old*dt./dx)*phi_old+dt*(theta*A_new*s_new+(1-theta)*A_old*s_old);
            else
                %t->inf:
                obj.A=theta*B_new./dx;
                obj.b=((1-theta)*B_old./dx)*phi_old+(theta*A_new*s_new+(1-theta)*A_old*s_old);
                %{
                Note: for theta=1 (Implicit Euler) this is exactly
                equivalent to dphi/dt=0.
                %}
            end
            %Boundary conditions
            Amd =zeros(m,m,N);
            Asub=zeros(m,m,N-1);
            Asup=zeros(m,m,N-1);
            
            Amd(:,:,1)=eye(m);
            Amd(:,:,end)=eye(m);
            
            %Apply Dirichlet BC
            %This is done by directly imposing the values of the last, and
            %first cells.
            obj.b(1:m)=grid_new.phi_BC_west;
            obj.b((N-1)*m+1:N*m)=grid_new.phi_BC_east;
            
            %Final assembly
            obj.A = obj.A + blktridiag(Amd,Asub,Asup);
            
            if isNewton
                %Finalize the construction for the quasi-Newton iteration
                %Here is assumed dA/dphi=0. 
                s_new_J_blockmat=blkdiag(grid_new.c_s_J{:});
                if isinf(dt)==false
                    obj.J=dt*theta*A_new*s_new_J_blockmat-obj.A;
                else
                    obj.J=theta*A_new*s_new_J_blockmat-obj.A;
                end
                obj.r=obj.b-obj.A*phi_new;
            end
        end
    end
    
end

