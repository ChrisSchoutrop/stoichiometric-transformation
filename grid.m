% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef grid < matlab.mixin.Copyable
    %{
        Handles everything related to the entire grid.
    
        Current settings:
        -equidistant nodes
        -1D
        -U,E position and phi dependent
    
        Gridpoints save transformed phi, not regular phi
    
        Th grid is layed out as:
        |--0--|--1--|--2--|--3--|--4--|--5--|
        Here the nodes are located at x0,x1,x2,x3,x4,x5.
        The physical grid runs from x_west=(x0+x1)/2 to x_east=(x4+x5)/2. To enforce the
        Dirichlet boundary conditions at x_west and x_east, the nodes x0 and x5
        are set as constants in the cells at the end. dx/2 on either side
        falls outside the domain.
    %}
    
    properties (Access=public)
        t
        N
        m
        xmin
        xmax
        dx
        U_fn
        E_fn
        s_fn
        s_fn_J  %Jacobian of the chemical source (only used if Newton iteration is performed)
        phi_BC_west_fn
        phi_BC_east_fn
        phi_BC_west
        phi_BC_east
        M
        
        vec_x   %Vector containing the x-coordinates of the nodes
        
        %Transformed quantities at cell centers
        c_phi
        c_s
        c_s_J
        c_U
        c_E
        
        %Discretization matrices, obtained using transformed and
        %interpolated U,E at the edges.
        v_alpha
        v_beta
        v_gamma
        v_delta
        
        %Transformed quantities at edges
        v_U
        v_E
        
    end %(Access=public)
    
    methods (Access=public)
        function obj=grid(input_struct)
            if nargin==0
                return
            end
            if isa(input_struct,'input_file')==false
                %This clause is needed to construct an empty object for
                %cloning the grid.
                return
            end
            %xmin,xmax,dx,U_fn,E_fn,s_fn
            obj.N=input_struct.N;
            obj.xmin=input_struct.xmin;
            obj.xmax=input_struct.xmax;
            obj.dx=input_struct.dx;
            obj.vec_x=linspace(obj.xmin,obj.xmax,obj.N)';
            obj.U_fn=input_struct.U_fn;
            obj.E_fn=input_struct.E_fn;
            obj.s_fn=input_struct.s_fn;
            if isempty(input_struct.s_fn_J)==false
                obj.s_fn_J=input_struct.s_fn_J;
            else
                %{
                If the Jacobian is not specified, use a numerical
                approximation. Only used in Quasi-Newton iterations, not
                Picard.
                %}
                obj.s_fn_J=@(t,x,phi) mf.Jacobian(@(phi) obj.s_fn(t,x,phi),phi);
            end
            obj.m=input_struct.m;
            obj.t=input_struct.tmin;
            
            obj.c_phi=mf.empty_vector_cells(obj.N,obj.m);
            obj.c_s=mf.empty_vector_cells(obj.N,obj.m);
            obj.c_s_J=mf.empty_matrix_cells(obj.N,obj.m);
            obj.c_U=mf.empty_matrix_cells(obj.N,obj.m);
            obj.c_E=mf.empty_matrix_cells(obj.N,obj.m);
            obj.v_U=mf.empty_matrix_cells(obj.N,obj.m);
            obj.v_E=mf.empty_matrix_cells(obj.N,obj.m);
            
            obj.v_alpha=mf.empty_matrix_cells(obj.N-1,obj.m);
            obj.v_beta=mf.empty_matrix_cells(obj.N-1,obj.m);
            obj.v_gamma=mf.empty_matrix_cells(obj.N-1,obj.m);
            obj.v_delta=mf.empty_matrix_cells(obj.N-1,obj.m);
            
            obj.M=input_struct.M;
            
            obj.phi_BC_west_fn=input_struct.phi_BC_west_fn;
            obj.phi_BC_east_fn=input_struct.phi_BC_east_fn;
            
            %Initialize the grid now that all settings are processed.
            obj.initialize_grid(input_struct);
        end
        function [obj]=set_dirichlet_BC(obj)
            %{
            Set the Dirichlet boundary conditions
            %}
            %Set the variable for the boundary conditions
            obj.phi_BC_west=obj.M*obj.phi_BC_west_fn(obj.t);
            obj.phi_BC_east=obj.M*obj.phi_BC_east_fn(obj.t);
            
            %Set the boundary values explicitly too
            obj.c_phi{1}=obj.phi_BC_west;
            obj.c_phi{obj.N}=obj.phi_BC_east;
        end
        function obj=update_grid(obj,t)
            %{
            Update the values stored in the grid points for new t,x,phi
            %}
            obj.t=t;
            obj.set_dirichlet_BC();
            
            %Update transformed quantities obtainable at the cell centers
            for iter=1:obj.N
                x=obj.vec_x(iter);
                
                phi_non_transformed=mf.inv_transform_vector(obj.c_phi{iter},obj.M);
                %U should not change under transform, since its a scalar matrix
                %obj.c_U{iter}=obj.U_fn(t,x,phi_non_transformed);
                obj.c_U{iter}=mf.transform_matrix(obj.U_fn(t,x,phi_non_transformed),obj.M);
                obj.c_E{iter}=mf.transform_matrix(obj.E_fn(t,x,phi_non_transformed),obj.M);
                
                obj.c_s{iter}=mf.transform_vector(obj.s_fn(t,x,phi_non_transformed),obj.M);
                if isempty(obj.s_fn_J)==false
                    obj.c_s_J{iter}=mf.transform_matrix(obj.s_fn_J(t,x,phi_non_transformed),obj.M);
                end
                
            end
            
            %Update quantities needed at the edges
            for iter=1:obj.N-1
                %{
                Use interpolated phi to compute the U and E on the edges,
                since at the ghost points phi can become negative or above
                1.
                %}
                %phi_non_transformed=obj.Minv*obj.c_phi{iter};
                phi_non_transformed_west=mf.inv_transform_vector(obj.c_phi{iter},obj.M);
                phi_non_transformed_east=mf.inv_transform_vector(obj.c_phi{iter+1},obj.M);
                phi_half=0.5*phi_non_transformed_west+0.5*phi_non_transformed_east;
                
                x=0.5*obj.vec_x(iter)+0.5*obj.vec_x(iter+1);
                %U should not change under transform, since it's a scalar matrix
                U_half=mf.transform_matrix(obj.U_fn(t,x,phi_half),obj.M);
                E_half=mf.transform_matrix(obj.E_fn(t,x,phi_half),obj.M);
                
                obj.v_U{iter}=U_half;
                obj.v_E{iter}=E_half;
                
                %{
                Calculate discretization matrices
                gamma, delta, alpha, beta
                using the interpolated, transformed U,E
                %}
                A=mf.A(U_half,E_half);
                P=mf.P(A,obj.dx);
                obj.v_alpha{iter} = mf.alpha(E_half,P,obj.dx);
                obj.v_beta{iter}  = mf.beta (E_half,P,obj.dx);
                Q=mf.Q(P,E_half);
                sigma=mf.sigma(E_half,A);
                obj.v_gamma{iter} = mf.gamma(Q,P,sigma);
                obj.v_delta{iter} = mf.delta(Q,P,sigma);
                
                %Because of the eigen-decompositions involved in
                %calculating the discretization matrices, they can have a
                %non-significant imaginary part.
                obj.v_alpha{iter} = real(obj.v_alpha{iter});
                obj.v_beta{iter}  = real(obj.v_beta{iter});
                obj.v_gamma{iter} = real(obj.v_gamma{iter});
                obj.v_delta{iter} = real(obj.v_delta{iter});
            end
            
        end
        function phi_vec=get_phi(obj)
            %Returns the untransformed phi vector
            phi_vec=NaN(obj.N*obj.m,1);
            for iter=1:obj.N
                phi_vec((iter-1)*obj.m+1:iter*obj.m)=(obj.M)\obj.c_phi{iter};
            end
        end
        function phi_vec=get_transformed_phi(obj)
            %Returns the transformed phi
            phi_vec=NaN(obj.N*obj.m,1);
            for iter=1:obj.N
                phi_vec((iter-1)*obj.m+1:iter*obj.m)=obj.c_phi{iter};
            end
        end
        function s_vec=get_transformed_source(obj)
            %Returns the transformed source vector
            s_vec=NaN(obj.N*obj.m,1);
            for iter=1:obj.N
                s_vec((iter-1)*obj.m+1:iter*obj.m)=obj.c_s{iter};
            end
        end
        function obj=set_phi(obj,phi_vec)
            %Input: untransformed phi as a single vector
            %Sets the transformed phi cells
            for iter=1:obj.N
                phi=phi_vec((iter-1)*obj.m+1:iter*obj.m);
                obj.c_phi{iter}=obj.M*phi;
            end
        end
        function obj=set_transformed_phi(obj,phi_vec)
            %Input: transformed phi as a single vector
            %Sets the transformed phi cells
            for iter=1:obj.N
                phi=phi_vec((iter-1)*obj.m+1:iter*obj.m);
                %phi=[phi(1);1;0];
                obj.c_phi{iter}=phi;
            end
        end
        function obj=initialize_grid(obj,input_struct)
            %{
            Set the initial values stored in the grid points.
            For initial phi guess take phi linearly interpolated between
            the boundary conditions.
            %}
            obj.set_phi(input_struct.phi_IC);
            obj.update_grid(input_struct.tmin);
            
        end
        function [obj]=plot_phi(obj,t,input_struct)
            %{
            Plot the variable phi for all gridpoints
            %}
            [phi_transformed,phi_untransformed,source_transformed,source_untransformed,x]=obj.transform_back();
            %{
            Output to the input_struct
            %}
            input_struct.x=x;
            input_struct.phi_transformed=phi_transformed;
            input_struct.phi_untransformed=phi_untransformed;
            input_struct.source_transformed=source_transformed;
            input_struct.source_untransformed=source_untransformed;
            
            if isempty(input_struct.particleList)==false
                %If there is no particle list, plot assumes phi is densities
                densities=zeros(size(phi_untransformed));
                for iter_convert=1:size(phi_untransformed,1)
                    densities(iter_convert,:)=stefan_maxwell.mass_frac_to_density(phi_untransformed(iter_convert,:),input_struct.particleList,input_struct.pressure);
                end
                
                densities=real(densities);
                densities_transformed=zeros(size(densities));
                for iter_convert=1:size(phi_untransformed,1)
                    densities_transformed(iter_convert,:)=(input_struct.M_unscaled*densities(iter_convert,:)')';
                end
            end
            
            %{
            Make the output plots (not intended for definite output)
            %}
            figure(1)
            clf
            p=plot(x,phi_transformed,'linewidth',1.5);
            set(gca,'linewidth',1.5)
            title('Transformed phi')
            xlabel('x'); ylabel('Transformed phi')
            legend(input_struct.transformed_labels)
            axis tight
            
            for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
                p(iter).Marker = '*';
            end
            
            figure(2)
            clf
            p=plot(x,phi_untransformed,'linewidth',1.5);
            set(gca,'linewidth',1.5)
            title('phi')
            xlabel('x'); ylabel('phi')
            legend(input_struct.labels)
            axis tight
            for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
                p(iter).Marker = '*';
            end
            
            figure(3)
            clf
            p=plot(x,source_transformed,'linewidth',1.5);
            set(gca,'linewidth',1.5)
            xlabel('x'); ylabel('source transformed')
            legend(input_struct.transformed_labels)
            axis tight
            for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
                p(iter).Marker = '*';
            end
            
            figure(4)
            clf
            p=plot(x,source_untransformed,'linewidth',1.5);
            set(gca,'linewidth',1.5)
            xlabel('x'); ylabel('source untransformed')
            legend(input_struct.labels)
            axis tight
            for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
                p(iter).Marker = '*';
            end
            
            %Plot the densities if applicable
%             if isempty(input_struct.particleList)==false
%                 figure(5)
%                 clf
%                 p=plot(x,densities_transformed,'linewidth',1.5);
%                 set(gca,'linewidth',1.5)
%                 title('Transformed densities')
%                 xlabel('x'); ylabel('Transformed densities')
%                 legend(input_struct.transformed_labels_unscaled)
%                 axis tight
%                 
%                 for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
%                     p(iter).Marker = '*';
%                 end
%                 
%                 figure(6)
%                 clf
%                 p=plot(x,densities,'linewidth',1.5);
%                 set(gca,'linewidth',1.5)
%                 title('densities')
%                 xlabel('x'); ylabel('densities')
%                 legend(input_struct.labels)
%                 axis tight
%                 for iter=input_struct.m-input_struct.Nelements+1:input_struct.m
%                     p(iter).Marker = '*';
%                 end
%             end
            %{
            Save phi and the sources to a file
            %}
            %dlmwrite('phi_transformed.txt',[x,phi_transformed],'delimiter','\t','precision',16)
            %dlmwrite('phi_untransformed.txt',[x,phi_untransformed],'delimiter','\t','precision',16)
            %dlmwrite('source_transformed.txt',[x,source_transformed],'delimiter','\t','precision',16)
            %dlmwrite('source_untransformed.txt',[x,source_untransformed],'delimiter','\t','precision',16)
            if isempty(input_struct.particleList)==false
                obj.conservations(input_struct);
            end
        end
        function [phi_transformed,phi_untransformed,source_transformed,source_untransformed,x]=transform_back(obj)
            %{
            Compute the untransformed variables
            %}
            phi_transformed=zeros(obj.N,obj.m);
            phi_untransformed=zeros(obj.N,obj.m);
            source_transformed=zeros(obj.N,obj.m);
            source_untransformed=zeros(obj.N,obj.m);
            x_temp=zeros(obj.N,1);
            for iter=1:obj.N
                x_temp(iter)=obj.vec_x(iter);
                phi_transformed(iter,:)=obj.c_phi{iter};
                source_transformed(iter,:)=obj.c_s{iter};
                phi_untransformed(iter,:)=mf.inv_transform_vector(obj.c_phi{iter},obj.M);
                source_untransformed(iter,:)=mf.inv_transform_vector(obj.c_s{iter},obj.M);
            end
            x=x_temp;
        end
        function [conservations_1_norm, conservations_inf_norm]=conservations(obj,input_struct)
            %{
            Print info on charge/element conservation
            %}
            conservations_1_norm=zeros(size(input_struct.elements.quantity,1),1);
            conservations_inf_norm=zeros(size(input_struct.elements.quantity,1),1);
            [phi_transformed,phi_untransformed,source_transformed,source_untransformed,x]=obj.transform_back();
            
            sigma=1-sum(phi_untransformed,2);
            %{
            Since there is no particle list, phi is assumed to be
            in terms of densities.
            %}
            disp('Mass Conservation:')
            disp(['Value on left boundary:',num2str(sigma(1),16)])
            disp(['Value on right boundary:',num2str(sigma(end),16)])
            disp(['1-Norm/N: ',num2str(norm(sigma,1)./obj.N,16)])
            disp(['Inf-Norm: ',num2str(norm(sigma,inf),16)])
            
            %{
            Output to the input_struct
            %}
            input_struct.x=x;
            input_struct.phi_transformed=phi_transformed;
            input_struct.phi_untransformed=phi_untransformed;
            input_struct.source_transformed=source_transformed;
            input_struct.source_untransformed=source_untransformed;
            input_struct.mass_conservation_1=norm(sigma,1)./obj.N;
            input_struct.mass_conservation_inf=norm(sigma,inf);
            
            if isempty(input_struct.particleList)==false
                densities=zeros(size(phi_untransformed));
                for iter_convert=1:size(phi_untransformed,1)
                    densities(iter_convert,:)=stefan_maxwell.mass_frac_to_density(phi_untransformed(iter_convert,:),input_struct.particleList,input_struct.pressure);
                end
                
                densities=real(densities);
                densities_transformed=zeros(size(densities));
                for iter_convert=1:size(phi_untransformed,1)
                    densities_transformed(iter_convert,:)=(input_struct.M_unscaled*densities(iter_convert,:)')';
                end
            end
            
            disp(' ')
            for iter=1:size(input_struct.elements.quantity,1)
                disp([input_struct.elements.description{iter},': '])
                
                
                if isempty(input_struct.particleList)==false
                    %{
                    If there is a particleList, then phi is expected in terms of
                    mass fractions.
                    This effectively checks:"How constant is this variable"
                    %}
                    mass_vec=zeros(size(input_struct.particleList));
                    for iter_mass=1:length(input_struct.particleList)
                        mass_vec(iter_mass)=input_struct.particleList{iter_mass}.mass;
                    end
                    %{
                        Idealy the value e1 would be constant.
                        e1=1-e1/max(abs(e1))
                    %}
                    e1=densities.*input_struct.elements.quantity(1,:);
                    %Sanity check:
                    %e1=densities.*rand(size(input_struct.elements.quantity(1,:)));
                    e1=sum(e1,2);
                    
%                     e1=(e1-e1(1))./abs(max(max(densities)));
%                     disp(['Value on left boundary:',num2str(e1(1),16)])
%                     disp(['Value on right boundary:',num2str(e1(end),16)])
                    
                    if strcmp(input_struct.elements.description{iter},'Charge conservation')
                        %e1=e1./(PhysConst.e.*sum(densities,2))
                        %q^Tn/(|q|^Tn)7
                        %e1=e1./(sum(densities.*abs(input_struct.elements.quantity(1,:)),2));
                        e1=e1./sum(densities,2); 
                        %=q^Tn/(e 1^Tn)
                        disp(['Value on left boundary:',num2str(e1(1),16)])
                        disp(['Value on right boundary:',num2str(e1(end),16)])                        
                        input_struct.charge_conservation_1=norm(e1,1)./obj.N;
                        input_struct.charge_conservation_inf=norm(e1,inf);
                    end
                    
                    conservations_1_norm(iter)=norm(e1,1)./obj.N;
                    conservations_inf_norm(iter)=norm(e1,inf);
                    disp(['1-Norm/N: ',num2str(conservations_1_norm(iter),16)])
                    disp(['Inf-Norm: ',num2str(conservations_inf_norm(iter),16)])
                    disp(' ')
                else
                    %{
                    Since there is no particle list, phi is assumed to be
                    in terms of densities.
                    %}
                    e1=sum(phi_untransformed.*input_struct.elements.quantity(iter,:),2);
                    disp(['Value on left boundary:',num2str(e1(1),16)])
                    disp(['Value on right boundary:',num2str(e1(end),16)])
                    disp('Conservation after subtraction of left boundary value:')
                    e1=e1-e1(1);
                    conservations_1_norm(iter)=norm(e1,1)./obj.N;
                    conservations_inf_norm(iter)=norm(e1,inf);
                    disp(['1-Norm/N: ',num2str(conservations_1_norm(iter),16)])
                    disp(['Inf-Norm: ',num2str(conservations_inf_norm(iter),16)])
                    disp(' ')
                end
            end
        end
    end %methods (Access=public)
end

