% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef input_file < handle
    %{
    Class that generates input_file structures. These structures can be
    used to set up the rest of the simulation with predefined settings.
    %}
    properties
        %{
        Overview of supported parameters:
        %}
        
        xmin    %Beginning of grid
        xmax    %End of grid
        dx      %Distance between nodal points
        x       %Vector containing the nodal points
        N       %N the number of gridpoints
        tmin    %Initial time
        tmax    %Final time
        dt      %Timestep size
        dt_auto=NaN;    %Keep relative change within this value, auto adjust timestep
        
        m           %Number of species
        Nelements   %Number of elements
        Nother      %Number of non-elements
        
        M           %Transformation matrix
        M_unscaled  %Transformation matrix without the mass scaling
        
        U_fn        %Function for the advection matrix U
        E_fn        %Function for the diffusion matrix E
        s_fn        %Function for the source vector s
        s_fn_J      %Function for the Jacobian of the source vector s
        
        phi_BC_west_fn  %Boundary condition west
        phi_BC_east_fn  %Boundary condition east
        phi_BC_west
        phi_BC_east
        phi_IC          %Initial condition
        
        tol_eta=1e-12;      %Tolerance element part
        tol_xi=1e-12;       %Tolerence non-element part
        tol_phi=1e-12;      %Tolerance full system
        max_iter_phi=100;   %Max iterations to solve full system
        max_iter_eta=1;     %Max iterations of element part per full(phi) iteration
        max_iter_xi=1;      %Max iterations of non-element part per full(phi) iteration
        max_iter_inner=1;   %Number of element/non-element iterations before continuing
        
        theta=1;            %Theta scheme parameter for time-integration:
        %0: Explicit Euler, 0.5 Trapezoidal, 1:
        %Implicit Euler
        urf_phi=1;          %Underrelaxation full(phi) system
        urf_eta=1;          %Underrelaxation element(eta) system
        urf_xi=1;           %Underrelaxation non-element(xi) system
        
        elements            %Struct to indicate elements
        labels              %Species labels
        transformed_labels  %Labels after transformation with M
        transformed_labels_unscaled
        
        particleList=[]     %List of particle structs
        pressure=[]         %Pressure in Pascal
        T_i=[]              %Ion temperature
        T_e=[]              %Electron temperature
        known_transformed=[]    %Transformed quantities which are known before hand, NaN=unknown, value=known
        
        phi                     %Info about the variable of interest
        phi_transformed         %Transformed variable of interest
        phi_untransformed       %Untransformed variable of interest
        source_transformed      %
        source_untransformed    %
        mass_conservation_1     %norm(sum(y),1)/N
        mass_conservation_inf   %norm(sum(y),inf)
        charge_conservation_1   %Charge conservation is computed from denties;
        %e1=(e1-e1(1))./abs(max(max(densities)));
        %Where e1 should be 0 idealy.
        charge_conservation_inf
        total_time              %Total time to compute the end-result
        mass_flux               %U-matrix halfway through the domain
        E_halfway               %Untransformed diffusion matrix halfway through the domain
        E_halfway_transformed
        phi_halfway
        x_halfway
        
        transform=[]
        
    end
    methods
        function obj=input_file(name)
            %{
            Construct the input file object from a filename.
            This copies over everything from the input file into the
            input_file object.
            %}
            input_struct=feval(str2func(name));
            input_properties = fieldnames(input_struct);
            for ix=1:length(input_properties)
                %Overwrite defaults if they are redefined in input_properties
                obj.(input_properties{ix})=input_struct.(input_properties{ix});
            end
            obj.transformed_labels=input_file.transform_labels(obj.labels,obj.M);
            obj.transformed_labels_unscaled=input_file.transform_labels(obj.labels,obj.M_unscaled);
            
        end
    end
    methods (Static)
        function density_vector=mass_frac_to_density(y,mass_vector,density)
            %{
            Converts mass fractions to number densities [1/m^3]
            density is the number density of all species combined;
            density=sum(density_vector)
            %}
            sigma_m=sum(y);
            density_vector=y.*density./(sigma_m.*mass_vector.*sum(y./mass_vector));
        end
        function k=arrhenius(c,q,E_a,T,Tref)
            %{
            Reaction rate given by the Arrhenius equation
            E_a : Activation energy in [J]
            c,q : parameters from literature
            T   : Temperature in [K]
            %}
            E_therm=PhysConst.k_B.*T;
            k=c.*(T./Tref).^q.*exp(-E_a./(E_therm));
        end
        function transformed_labels=transform_labels(labels,M)
            %{
            Transforms species labels to labels for the transformed
            species. This effectively performs the matrix multiplication
            "transformed_labels=M*labels".
            %}
            if isempty(labels)
                transformed_labels=[]
                return;
            end
            transformed_labels=cell(size(labels));
            for i=1:size(M,1)
                transformed_labels{i}='';
                for j=1:size(M,2)
                    M_value=M(i,j);
                    if M_value==0
                        continue;
                    elseif M_value==1
                        if isempty(transformed_labels{i})
                            transformed_labels{i}=[transformed_labels{i},'',labels{j}];
                        else
                            transformed_labels{i}=[transformed_labels{i},'+',labels{j}];
                        end
                    elseif M_value==-1
                        transformed_labels{i}=[transformed_labels{i},'-',labels{j}];
                    else
                        if isempty(transformed_labels{i})
                            transformed_labels{i}=[transformed_labels{i},'',num2str(M_value),labels{j}];
                        else
                            transformed_labels{i}=[transformed_labels{i},'+',num2str(M_value),labels{j}];
                        end
                    end
                end
            end
        end
    end
end
