% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function [json_string]=input_struct_to_json(input_struct,json_input_filename)

%{
Convert function handles to strings
%}
input_struct.U_fn = func2str(input_struct.U_fn);
input_struct.E_fn = func2str(input_struct.E_fn);
input_struct.s_fn = func2str(input_struct.s_fn);
try
    input_struct.s_fn_J = func2str(input_struct.s_fn_J);
catch
    input_struct.s_fn_J=NaN;
end
input_struct.phi_BC_west_fn = func2str(input_struct.phi_BC_west_fn);
input_struct.phi_BC_east_fn = func2str(input_struct.phi_BC_east_fn);

%Encode to json string
json_string=jsonencode(input_struct);

%{
Write to file
%}
fid=fopen(json_input_filename,'wt');
fprintf(fid,json_string);
fclose(fid);
end
