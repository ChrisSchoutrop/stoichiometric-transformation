% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)

function obj=eight_species(obj)

%{
            Input parameters
%}

obj.transform=true;
obj.theta=1; %{0: Explicit Euler, 0.5 Trapezoidal, 1: Implicit Euler}
obj.xmin=0;
obj.xmax=0.0010;
obj.dx=(obj.xmax-obj.xmin)/128;
obj.xmin=obj.xmin;
obj.xmax=obj.xmax;
obj.tmin=0;
obj.tmax=inf;
obj.dt=1;
obj.dt_auto=NaN; %time_resolution/timescale
obj.labels={'Ar^+','Ar^*','Al^+','H','e^-','Ar','Al','H_2'};

obj.elements.quantity=[
    1 0 1 0 -1 0 0 0;...
    0 0 1 0 0 0 1 0;...
    1 1 0 0 0 1 0 0;...
    0 0 0 1 0 0 0 2];
obj.elements.description={'Charge conservation','Aluminium conservation','Argon conservation','Hydrogen conservation'};

T_e=16000;
T_i=3500;
obj.pressure=5;
mass_flux=-0.05;
obj.mass_flux=mass_flux;


obj.particleList{1}=molecular_database('Ar[+1]');
obj.particleList{2}=molecular_database('Ar[*]');
obj.particleList{3}=molecular_database('Al[+1]');
obj.particleList{4}=molecular_database('H');
obj.particleList{5}=molecular_database('e[-1]');
obj.particleList{6}=molecular_database('Ar');
obj.particleList{7}=molecular_database('Al');
obj.particleList{8}=molecular_database('H2');

obj.particleList{1}.temperature=T_i;
obj.particleList{2}.temperature=T_i;
obj.particleList{3}.temperature=T_i;
obj.particleList{4}.temperature=T_i;
obj.particleList{5}.temperature=T_e;
obj.particleList{6}.temperature=T_i;
obj.particleList{7}.temperature=T_i;
obj.particleList{8}.temperature=T_i;

%{
            Derived quantities
%}
obj.m=length(obj.particleList); %Number of species
mass_vector=zeros(obj.m,1);
for iter=1:obj.m
    mass_vector(iter)=obj.particleList{iter}.mass;
end

y_east =[...
    0.0081;...
    0.11;...
    0.094;...
    0.26;...
    2.0e-06;...
    0.22;...
    0.25;...
    0.054;...
    ];
y_west =[...
    0.16;...
    0.11;...
    0.13;...
    0.13;...
    4.9e-06;...
    0.19;...
    0.22;...
    0.046;...
    ];

%Quasineutrality on the boundaries
y_east(5)=mass_vector(5)*(y_east(1)/mass_vector(1)+y_east(3)/mass_vector(3));



y_west(5)=mass_vector(5)*(y_west(1)/mass_vector(1)+y_west(3)/mass_vector(3));

y_east=y_east/sum(y_east);
y_west=y_west/sum(y_west);

obj.x=[obj.xmin:obj.dx:obj.xmax];
obj.N=length(obj.x);

obj.Nelements=size(obj.elements.quantity,1);

%Control method
obj.M=eye(obj.m);
%basis method
if obj.transform
    obj.M=[
        +1 +0 +0 +0   +1 -1 +0   +0;...
        +0 +1 +0 +0   +0 -1 +0   +0;...
        +0 +0 +1 +0   +1 +0 -1   +0;...
        +0 +0 +0 +1   +0 +0 +0   -0.5;...
        -1 +0 -1 +0   +1 +0 +0   +0;...
        +1 +1 +0 +0   +0 +1 +0   +0;...
        +0 +0 +1 +0   +0 +0 +1   +0;...
        +0 +0 +0 +0.5 +0 +0 +0   +1;...
        ];
    obj.known_transformed=[NaN;NaN;NaN;NaN;0;1;NaN;NaN];
end

nu=[...
    +1 -1 +0 +1 +0 +0 +0 +0 -1 +0;...
    +0 +0 +1 -1 -1 -1 +0 -1 +0 +0;...
    +0 +0 +0 +0 +0 +0 +1 +1 +1 +0;...
    +0 +0 +0 +0 +0 +0 +0 +0 +0 +2;...
    +1 -1 +0 +1 +0 +0 +1 +1 +0 +0;...
    -1 +1 -1 +0 +1 +1 +0 +1 +1 +0;...
    +0 +0 +0 +0 +0 +0 -1 -1 -1 +0;...
    +0 +0 +0 +0 +0 +0 +0 +0 +0 -1;...
    ];

obj.M_unscaled=obj.M;
U=diag(mass_flux*ones(obj.m,1));
obj.U_fn=@(t,x,phi) U;

obj.E_fn=@(t,x,phi) stefan_maxwell.diff_matrix(phi,obj.pressure,obj.particleList);

%{
            Scale M with the masses
%}

if obj.transform
    M2=...
        [...
        1 0 0 0 0 0 0 0;...
        0 1 0 0 0 0 0 0;...
        0 0 1 0 0 0 0 0;...
        0 0 0 1 0 0 0 0;...
        0 0 0 0 1 0 0 0;...
        0 0 0 0 1 1 1 1;...
        0 0 0 0 0 0 1 0;...
        0 0 0 0 0 0 0 1;...
        ];
    obj.M=diag(mass_vector)*obj.M/diag(mass_vector);
    obj.M=M2*obj.M; %Ttilde
else
    obj.M=eye(obj.m);
end

%clear c,q,E_a,T,Tref
T_ref_e=PhysConst.eVT; %[K]
T_ref_i=1; %[K]

%Argon ionization
k(1 )=input_file.arrhenius(2.3e-14,0.68,15.75.*PhysConst.eV,T_e,T_ref_e);

%Argon recombination
k(2 )=input_file.arrhenius(8.75e-39,-4.5,0,T_e,T_ref_e);

%Argon excitation
k(3 )=input_file.arrhenius(5.0e-15,0.74,11.56.*PhysConst.eV,T_e,T_ref_e);

%Ionization of excited argon
k(4 )=input_file.arrhenius(6.8e-15,0.67,4.20.*PhysConst.eV,T_e,T_ref_e);

%Argon de-excitation
k(5 )=input_file.arrhenius(4.3e-16,0.74,0,T_e,T_ref_e);

%Radiative de-excitation of argon (Note: This is independent of
%temperature)
k(6 )=input_file.arrhenius(3.15e8,0,0,1,1);

%Aluminium ionization (Note: q=0 for this reaction, no T_ref is needed)
k(7 )=input_file.arrhenius(1.23e-13,0,7.23.*PhysConst.eV,T_e,1);

%Penning ionization (Note: This is independent of
%temperature)
k(8 )=input_file.arrhenius(5.9e-16,0,0,1,1);

%Al-Ar Charge exchange (Note: This is independent of
%temperature)
k(9 )=input_file.arrhenius(1.0e-15,0,0,1,1);

%Dissociation of hydrogen (Note: T_ref_e is correct, original author
%indicated temperatures in eV for these coefficients).
k(10)=input_file.arrhenius(5.60e-15,0.419,4.478.*PhysConst.eV,T_i,T_ref_e);

Z_density=@(density)...
    [...
    k(1 ).*density(6).*density(5);...
    k(2 ).*density(1).*density(5).*density(5);...
    k(3 ).*density(6).*density(5);...
    k(4 ).*density(2).*density(5);...
    k(5 ).*density(2).*density(5);...
    k(6 ).*density(2);...
    k(7 ).*density(7).*density(5);...
    k(8 ).*density(2).*density(7);...
    k(9 ).*density(1).*density(7);...
    k(10).*density(8).*density(8);...
    ];

Z_mass_frac= @(phi) Z_density(stefan_maxwell.mass_frac_to_density(phi,obj.particleList,obj.pressure));

obj.s_fn=@(t,x,phi) mass_vector.*(nu*Z_mass_frac(phi./sum(phi)));

obj.phi_BC_west_fn=@(t) y_west;
obj.phi_BC_east_fn=@(t) y_east;
obj.phi_IC=NaN(obj.N*obj.m,1);
%{
            Start with a linear profile as an initial guess
%}
dydx=(obj.phi_BC_east_fn(0)-obj.phi_BC_west_fn(0))./(obj.xmax-obj.xmin);
for iter=1:obj.N
    x_point=obj.x(iter);
    obj.phi_IC((iter-1)*obj.m+1:(iter)*obj.m)=...
        dydx.*(x_point-obj.xmin)+obj.phi_BC_west_fn(0);
end

obj.tol_phi=1e-7
obj.max_iter_phi=500;
obj.urf_phi=1;

obj.transformed_labels=input_file.transform_labels(obj.labels,obj.M);
end
