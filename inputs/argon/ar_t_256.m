% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function obj=decent_ar_ex(obj)

%{
            Input parameters
%}

rng(5)
obj.transform=true
obj.theta=1; %{0: Explicit Euler, 0.5 Trapezoidal, 1: Implicit Euler}
obj.xmin=0;
obj.xmax=1e-4;
obj.dx=(obj.xmax-obj.xmin)/256;
obj.xmin=obj.xmin;
obj.xmax=obj.xmax;
obj.tmin=0;
obj.tmax=inf;
obj.dt=1;
obj.labels={'Ar^+','Ar','e^-'};

obj.elements.quantity=[
    1 0 -1;
    1 1 0];
obj.elements.description={'Charge conservation','Argon Conservation'};

T_i=300;
T_e=7500;
obj.pressure=1e3;
mass_flux=-0.1;
y_west=[0.04;0.96];
y_east=[0.96;0.04];
obj.mass_flux=mass_flux;
obj.T_e=T_e;
obj.T_i=T_i;

%Transformation matrix

obj.M=eye(3);
if obj.transform
    
    obj.known_transformed=[NaN;1;0]
    obj.M=[1 -1 1; 1 1 0; -1 0 1];
end


obj.M_unscaled=obj.M;
obj.particleList{1}=molecular_database('Ar[+1]');
obj.particleList{2}=molecular_database('Ar');
obj.particleList{3}=molecular_database('e[-1]');

%Calculate the electron fraction needed for charge conservation
y_west_electron=obj.particleList{3}.mass.*(y_west(1)./obj.particleList{1}.mass);
y_east_electron=obj.particleList{3}.mass.*(y_east(1)./obj.particleList{1}.mass);
y_west=[y_west;y_west_electron];
y_west=y_west/sum(y_west);
y_east=[y_east;y_east_electron];
y_east=y_east/sum(y_east);

obj.particleList{1}.temperature=T_i;
obj.particleList{2}.temperature=T_i;
obj.particleList{3}.temperature=T_e;

%{
            Derived quantities
%}
obj.x=[obj.xmin:obj.dx:obj.xmax];
obj.N=length(obj.x);
obj.m=size(obj.elements.quantity,2); %Number of species
obj.Nelements=size(obj.elements.quantity,1);

%{
            Advection matrix

%}
U=diag(mass_flux*ones(3,1));

obj.U_fn=@(t,x,phi) U;

obj.E_fn=@(t,x,phi) stefan_maxwell.diff_matrix(phi,obj.pressure,obj.particleList);


nu=[1; -1;1];

%{
            Scale M with the masses
%}

mass_vector=zeros(obj.m,1);
for iter=1:obj.m
    mass_vector(iter)=obj.particleList{iter}.mass;
end
obj.M_unscaled=obj.M;
obj.M=diag(mass_vector)*obj.M/diag(mass_vector);
if obj.transform
    M2=...
        [...
        1 0 0;...
        0 1 1;...
        0 0 1;...
        ]
    obj.M=M2*obj.M;
else
    obj.M=eye(obj.m);
end

%Reactions:
%{
            Ar ionization
            Ar + e[-1] -> Ar[+1] + 2e[-1]
%}
c = 2.3e-14;
q = 0.68;
E_a = 15.76*PhysConst.eV;
Tref = PhysConst.eV/PhysConst.k_B; %1 eV in K
k_ionization=input_file.arrhenius(c,q,E_a,T_e,Tref); % m^3/s
%{
            Ar recombination
            Ar[+1] + 2e[-1] -> Ar + e[-1]
%}
c = 8.75e-39;
q = -4.5;
E_a = 0;
Tref = PhysConst.eV/PhysConst.k_B;
k_recombination=input_file.arrhenius(c,q,E_a,T_e,Tref); % m^6/s

Z_density= @(n) [k_ionization.*n(2)*n(3)-k_recombination.*n(1).*n(3)*n(3)];

Z_mass_frac= @(phi) Z_density(stefan_maxwell.mass_frac_to_density(phi,obj.particleList,obj.pressure));

obj.s_fn=@(t,x,phi) diag(mass_vector)*nu*Z_mass_frac(phi./sum(phi));
obj.s_fn_J=@(t,x,phi) mf.Jacobian(@(phi) obj.s_fn(t,x,phi),phi);

y_west=y_west./sum(y_west); %Mass conservation
y_east=y_east./sum(y_east); %Mass conservation
bc_west=obj.M\y_west;
bc_east=obj.M\y_east;

obj.phi_BC_west_fn=@(t) y_west;
obj.phi_BC_east_fn=@(t) y_east;
obj.phi_IC=NaN(obj.N*obj.m,1);
%{
            Start with a linear profile as an initial guess
%}
dydx=(obj.phi_BC_east_fn(0)-obj.phi_BC_west_fn(0))./(obj.xmax-obj.xmin);
for iter=1:obj.N
    x_point=obj.x(iter);
    obj.phi_IC((iter-1)*obj.m+1:(iter)*obj.m)=...
        dydx.*(x_point-obj.xmin)+obj.phi_BC_west_fn(0);
end
obj.tol_phi=1e-7
obj.max_iter_phi=250;
obj.urf_phi=1.0;

obj.transformed_labels=input_file.transform_labels(obj.labels,obj.M);
end
