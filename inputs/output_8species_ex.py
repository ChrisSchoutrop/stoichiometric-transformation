import numpy as np
import json
import matplotlib.pyplot as plt
from matplotlib import pyplot
from matplotlib import rc
from multiprocessing import Pool

name = '8species_ex'

input_file = 'output_'+name+'.json'
with open(input_file) as json_file:
    data = json.load(json_file)
print(data)

rc('font', **{'family': 'normal', 'weight': 'normal', 'size': 18})

def plot_phi():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['phi_untransformed'])
    plt.plot(data['x'], y[:,0],'-' ,color='#1f77b4')
    plt.plot(data['x'], y[:,1],'-' ,color='#ff7f0e')
    plt.plot(data['x'], y[:,2],'-' ,color='#2ca02c')
    plt.plot(data['x'], y[:,3],'-' ,color='#d62728')
    plt.plot(data['x'], y[:,4],'-*',color='#1f77b4')
    plt.plot(data['x'], y[:,5],'-*',color='#ff7f0e')
    plt.plot(data['x'], y[:,6],'-*',color='#2ca02c')
    plt.plot(data['x'], y[:,7],'-*',color='#d62728')
    labels = ["$y_{\\mathrm{Ar}^+}$","$y_{\\mathrm{Ar}^*}$","$y_{\\mathrm{Al}^+}$","$y_{\\mathrm{H}}$","$y_{\\mathrm{e}^-}$","$y_{\\mathrm{Ar}}$","$y_{\\mathrm{Al}}$","$y_{\\mathrm{H}_2}$"]
    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='both', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Mass fractions [ ]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_phi.pdf", bbox_inches='tight')
    f.savefig(name+"_phi.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_phi_transformed():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['phi_transformed'])
    plt.plot(data['x'], y[:,0],'-' ,color='#1f77b4')
    plt.plot(data['x'], y[:,1],'-' ,color='#ff7f0e')
    plt.plot(data['x'], y[:,2],'-' ,color='#2ca02c')
    plt.plot(data['x'], y[:,3],'-' ,color='#d62728')
    plt.plot(data['x'], y[:,4],'-*',color='#1f77b4')
    plt.plot(data['x'], y[:,5],'-*',color='#ff7f0e')
    plt.plot(data['x'], y[:,6],'-*',color='#2ca02c')
    plt.plot(data['x'], y[:,7],'-*',color='#d62728')
    labels=[]
    for i in range(1,9):
        labels.append('$[\\widetilde{\\mathbf{T}}y]_'+str(i)+'$')

    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='both', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Transformed mass fractions [ ]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_phi2.pdf", bbox_inches='tight')
    f.savefig(name+"_phi2.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_source():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['source_untransformed'])
    plt.plot(data['x'], y[:,0],'-' ,color='#1f77b4')
    plt.plot(data['x'], y[:,1],'-' ,color='#ff7f0e')
    plt.plot(data['x'], y[:,2],'-' ,color='#2ca02c')
    plt.plot(data['x'], y[:,3],'-' ,color='#d62728')
    plt.plot(data['x'], y[:,4],'-*',color='#1f77b4')
    plt.plot(data['x'], y[:,5],'-*',color='#ff7f0e')
    plt.plot(data['x'], y[:,6],'-*',color='#2ca02c')
    plt.plot(data['x'], y[:,7],'-*',color='#d62728')
    labels = ["$s_{\mathrm{Ar}^+}$","$s_{\mathrm{Ar}^*}$","$s_{\mathrm{Al}^+}$","$s_\mathrm{H}$","$s_{\mathrm{e}^-}$","$s_\mathrm{Ar}$","$s_\mathrm{Al}$","$s_{\mathrm{H}_2}$"]
    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='both', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Source term [kgs$^{-1}$m$^{-3}$]', usetex=False)
    plt.yscale('symlog')
    f.savefig(name+"_source.pdf", bbox_inches='tight')
    f.savefig(name+"_source.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_source_transformed():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['source_transformed'])
    plt.plot(data['x'], y[:,0],'-' ,color='#1f77b4')
    plt.plot(data['x'], y[:,1],'-' ,color='#ff7f0e')
    plt.plot(data['x'], y[:,2],'-' ,color='#2ca02c')
    plt.plot(data['x'], y[:,3],'-' ,color='#d62728')
    plt.plot(data['x'], y[:,4],'-*',color='#1f77b4')
    plt.plot(data['x'], y[:,5],'-*',color='#ff7f0e')
    plt.plot(data['x'], y[:,6],'-*',color='#2ca02c')
    plt.plot(data['x'], y[:,7],'-*',color='#d62728')
    labels=[]
    for i in range(1,9):
        labels.append('$\\overline{s}_'+str(i)+'$')
    plt.legend(labels, loc='center right')
    ax.autoscale(enable=True, axis='both', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Transformed Source term [kgs$^{-1}$m$^{-3}$]', usetex=False)
    plt.yscale('symlog')
    f.savefig(name+"_source2.pdf", bbox_inches='tight')
    f.savefig(name+"_source2.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)


pool = Pool(processes=4)
pool.apply_async(plot_source)
pool.apply_async(plot_source_transformed)
pool.apply_async(plot_phi)
pool.apply_async(plot_phi_transformed)
pool.close()
pool.join()