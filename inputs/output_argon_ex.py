import numpy as np
import json
import matplotlib.pyplot as plt
from matplotlib import pyplot
from matplotlib import rc
from multiprocessing import Pool

name = 'argon_ex'

input_file = 'output_'+name+'.json'
with open(input_file) as json_file:
    data = json.load(json_file)
print(data)

rc('font', **{'family': 'normal', 'weight': 'normal', 'size': 18})

def plot_phi():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['phi_untransformed'])
    plt.plot(data['x'], y[:,0])
    plt.plot(data['x'], y[:,1])
    plt.plot(data['x'], y[:,2]*1e4)
    labels = ['$y_{\mathrm{Ar}^{+}}$','$y_\mathrm{Ar}$','$10^4\\times y_{\mathrm{e}^{-}}$']
    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.autoscale(enable=True, axis='y', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Mass fractions [ ]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_phi.pdf", bbox_inches='tight')
    f.savefig(name+"_phi.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_phi_transformed():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['phi_transformed'])
    plt.plot(data['x'], y[:,0])
    plt.plot(data['x'], y[:,1])
    plt.plot(data['x'], y[:,2])
    #labels = ['Ar$^+$-1.0 Ar+$7.3\\times10^4$e$^-$','1.0Ar$^+$+Ar','-$1.4\\times 10^{-5}$Ar$^+$+e$^-$']
    labels = ['$[\widetilde{\\mathbf{T}}y]_1$','$[\widetilde{\\mathbf{T}}y]_2$','$[\widetilde{\\mathbf{T}}y]_3$']
    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.autoscale(enable=True, axis='y', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Transformed mass fractions [ ]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_phi2.pdf", bbox_inches='tight')
    f.savefig(name+"_phi2.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_source():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['source_untransformed'])
    plt.plot(data['x'], y[:,0])
    plt.plot(data['x'], y[:,1])
    plt.plot(data['x'], y[:,2]*1e4)
    labels = ['$s_{\mathrm{Ar}^{+}}$','$s_\mathrm{Ar}$','$10^4\\times s_{\mathrm{e}^{-}}$']
    plt.legend(labels, loc=1)
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.autoscale(enable=True, axis='y', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Source term [kgs$^{-1}$m$^{-3}$]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_source.pdf", bbox_inches='tight')
    f.savefig(name+"_source.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

def plot_source_transformed():
    f = plt.figure()
    ax = plt.gca()
    y=np.array(data['source_transformed'])
    plt.plot(data['x'], y[:,0])
    plt.plot(data['x'], y[:,1]*1e16)
    plt.plot(data['x'], y[:,2]*1e16)
    labels = ['$\\overline{s}_1$','$10^{16}\\times\\overline{s}_2$','$10^{16}\\times\\overline{s}_3$']
    plt.legend(labels, loc='center right')
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.autoscale(enable=True, axis='y', tight=False)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                top=True, left=True, right=True)
    ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
    ax.ticklabel_format(axis='y', style='plain')
    plt.xlabel('x [m]', usetex=False)
    plt.ylabel('Transformed Source term [kgs$^{-1}$m$^{-3}$]', usetex=False)
    plt.yscale('linear')
    f.savefig(name+"_source2.pdf", bbox_inches='tight')
    f.savefig(name+"_source2.png", bbox_inches='tight')
    #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
    plt.close(f)

pool = Pool(processes=4)
pool.apply_async(plot_source)
pool.apply_async(plot_source_transformed)
pool.apply_async(plot_phi)
pool.apply_async(plot_phi_transformed)
pool.close()
pool.join()