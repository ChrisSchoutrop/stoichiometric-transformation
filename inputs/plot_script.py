#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 16:15:10 2019

@author: chris
"""

import numpy as np
import json
import matplotlib.pyplot as plt
from matplotlib import pyplot
from matplotlib import rc
import os
import pickle
from multiprocessing import Pool, cpu_count
from termcolor import colored, cprint
# Save plots as editable object
# https://stackoverflow.com/questions/4348733/saving-interactive-matplotlib-figures
# import plot_positions

process_parallel = True

# Set global plot properties
rc('font', **{'family': 'normal', 'weight': 'normal', 'size': 20})
params = {'text.latex.preamble': [r'\usepackage{amsmath}']}
pyplot.rcParams.update(params)
replacement_label = {
    "Ar": "\\text{Ar}",
    "Ar^+": "\\text{Ar}^+",
    "e^-": "\\text{e}^-",
    "Ar^++-72823.3962e^-": "\\text{Ar}^+-7.3\\times 10^5\\text{e}^-",
    "1.3732e-05Ar+1.3732e-05Ar^+": "1.4\\times10^{-5}\\text{Ar}+1.4\\times10^{-5}\\text{Ar}^+",
    "sigmam1":r"\|\|\sigma_{\mathrm{m}}\|\|_{1}/N",
    "sigmaminf":r"\|\|\sigma_{\mathrm{m}}\|\|_{\infty}",
    "sigmac1":r"\|\|\sigma_{\mathrm{c}}\|\|_{1}/N",
    "sigmacinf":r"\|\|\sigma_{\mathrm{c}}\|\|_{\infty}",
}
plot_names = {
    'cond_scaled': r'$\kappa(\mathbf{QAP})$',
    'cond_unscaled': r'$\kappa(\mathbf{A})$',
    'residual': 'Relative residual of linear system',
    'error': 'Lowest reached residual of solving phi-equation',
    'final_residual': 'Last residual of solving phi-equation ',
    'final_update_size': 'Last update_size of solving phi-equation ',
    'iterations': 'Number of iterations',
    'solve_time': 'CPU time [s]',
}
# "sigmam1":r"\|\sigma_\\text{m}\|_{1}/N",
# "sigmaminf":r"\\|\\sigma_\\text{m}\\|_{\\infty}",
# "sigmac1":r"\\|\\sigma_\\text{c}\\|_{1}/N'",
# "sigmacinf":r"\\|\\sigma_\\text{c}\\|_{\\infty}",
# plot_names_others = {
#     'mass_conservation_1': 'Conservation of mass',
#     'mass_conservation_inf': 'Conservation of mass',
#     'charge_conservation_1': 'Quasi-neutrality',
#     'charge_conservation_inf': 'Quasi-neutrality',
#     'total_time': 'Time [s]',
# }
plot_names_others = {
    'mass_conservation_1': 'sigmam1',
    'mass_conservation_inf': 'sigmaminf',
    'charge_conservation_1': 'sigmac1',
    'charge_conservation_inf': 'sigmacinf',
    'total_time': 'Time [s]',
}

'''
Input folders
'''
folders = ['8species', 'argon']


def change_ghost_to_center(input):
    '''
    Converts from the ghost-cell construction to boundaries on the vertices at the ends.
    '''
    #west boundary
    input[0] = (input[0]+input[1])/2
    #east boundary
    input[-1] = (input[-1]+input[-2])/2
    return input


def plot_position(folder):
    '''
    Plots all things that depend on position
    '''
    # Get a list of files in a folder
    files = []
    for file in os.listdir(folder):
        if file.endswith(".json"):
            files.append(os.path.join(folder, file))

    # Load the files into memory
    all_data = []
    for file in files:
        with open(file) as json_file:
            all_data.append(json.load(json_file))

    for data_index, data in enumerate(all_data):
        #Interpolate the ghost cell values to real boundaries
        data['x'] = change_ghost_to_center(np.array(data['x']))
        data['phi_untransformed'] = change_ghost_to_center(
            np.array(data['phi_untransformed']))
        data['phi_transformed'] = change_ghost_to_center(
            np.array(data['phi_transformed']))
        data['source_untransformed'] = change_ghost_to_center(
            np.array(data['source_untransformed']))
        data['source_transformed'] = change_ghost_to_center(
            np.array(data['source_transformed']))
    '''
	Position plots
	'''
    rc('text', usetex=True)

    # Plot of phi as function of x
    for data_index, data in enumerate(all_data):
        f = plt.figure()
        ax = plt.gca()
        plt.plot(data['x'], data['phi_untransformed'])
        labels = []
        for index, label in enumerate(data['labels']):
            if label in replacement_label:
                label = replacement_label[label]
                print(label)
            labels.append('$'+label+'$')
        print(labels)
        plt.legend(labels, loc='best')
        ax.autoscale(enable=True, axis='both', tight=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
        ax.tick_params(axis='both', direction='in', bottom=True,
                       top=True, left=True, right=True)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        ax.ticklabel_format(axis='y', style='plain')
        plt.xlabel('x [m]', usetex=False)
        plt.ylabel('Mass fractions [ ]', usetex=False)
        plt.yscale('log')
        f.savefig(files[data_index]+"_phi.pdf", bbox_inches='tight')
        f.savefig(files[data_index]+"_phi.png", bbox_inches='tight')
        #pickle.dump(f, open(files[data_index]+"_phi.pickle", 'wb'))
        plt.close(f)

    # Plot of transformed phi as function of x
    for data_index, data in enumerate(all_data):
        f = plt.figure()
        ax = plt.gca()
        plt.plot(data['x'], data['phi_transformed'])
        labels = []
        for index, label in enumerate(data['transformed_labels']):
            if label in replacement_label:
                label = replacement_label[label]
            labels.append('$'+label+'$')
        plt.legend(labels, loc='best')
        ax.autoscale(enable=True, axis='both', tight=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
        ax.tick_params(axis='both', direction='in', bottom=True,
                       top=True, left=True, right=True)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        ax.ticklabel_format(axis='y', style='plain')
        plt.xlabel('x [m]', usetex=False)
        plt.ylabel('Mass fractions [ ]', usetex=False)
        plt.yscale('log')
        f.savefig(files[data_index]+"_phi2.pdf", bbox_inches='tight')
        f.savefig(files[data_index]+"_phi2.png", bbox_inches='tight')
        #pickle.dump(f, open(files[data_index]+"_phi2.pickle", 'wb'))
        plt.close(f)

    # Plot of transformed source as function of x
    for data_index, data in enumerate(all_data):
        f = plt.figure()
        ax = plt.gca()
        plt.plot(data['x'], data['source_transformed'])
        labels = []
        for index, label in enumerate(data['transformed_labels']):
            if label in replacement_label:
                label = replacement_label[label]
            labels.append('$'+label+'$')
        plt.legend(labels, loc='best')
        ax.autoscale(enable=True, axis='both', tight=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
        ax.tick_params(axis='both', direction='in', bottom=True,
                       top=True, left=True, right=True)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        ax.ticklabel_format(axis='y', style='plain')
        plt.xlabel('x [m]', usetex=False)
        plt.ylabel('Transformed sources [1/s]', usetex=False)
        plt.yscale('log')
        f.savefig(files[data_index]+"_s2.pdf", bbox_inches='tight')
        f.savefig(files[data_index]+"_s2.png", bbox_inches='tight')
        #pickle.dump(f, open(files[data_index]+"_s2.pickle", 'wb'))
        plt.close(f)

    # Plot of source as function of x
    for data_index, data in enumerate(all_data):
        f = plt.figure()
        ax = plt.gca()
        plt.plot(data['x'], data['source_untransformed'])
        labels = []
        for index, label in enumerate(data['labels']):
            if label in replacement_label:
                label = replacement_label[label]
            labels.append('$'+label+'$')
        plt.legend(labels, loc='best')
        ax.autoscale(enable=True, axis='both', tight=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
        ax.tick_params(axis='both', direction='in', bottom=True,
                       top=True, left=True, right=True)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        ax.ticklabel_format(axis='y', style='plain')
        plt.xlabel('x [m]', usetex=False)
        plt.ylabel('Sources [1/s]', usetex=False)
        plt.yscale('linear')
        f.savefig(files[data_index]+"_s.pdf", bbox_inches='tight')
        f.savefig(files[data_index]+"_s.png", bbox_inches='tight')
        #pickle.dump(f, open(files[data_index]+"_s.pickle", 'wb'))
        plt.close(f)


def plot_N(folder):
    '''
    Plots all things that depend on N
    '''
    # Get a list of files in a folder
    files = []
    for file in os.listdir(folder):
        if file.endswith(".json"):
            files.append(os.path.join(folder, file))

    # Load the files into memory
    all_data = []
    for file in files:
        with open(file) as json_file:
            all_data.append(json.load(json_file))

    rc('text', usetex=False)
    plot_quantities_phi = ['cond_scaled', 'cond_unscaled',
                           'residual', 'error', 'final_residual','final_update_size' ,'iterations','solve_time']
    # plot_quantities = ['mass_conservation_1', 'mass_conservation_inf',
    #                    'charge_conservation_1', 'charge_conservation_inf', 'total_time']
    plot_names.update(plot_names_others)
    # Figure out how many different transformations exist, and which ones
    list_of_M = []
    for data_index, data in enumerate(all_data):
        already_known_M = False
        for M in list_of_M:
            if data["M_unscaled"] == M:
                already_known_M = True
                break
        if already_known_M == False:
            list_of_M.append(data["M_unscaled"])

    # Make number_of_transformations lists
    # Load in the quantity of interest as function of N from the data
    number_of_transformations = len(list_of_M)

    '''
    Plot the quantities that depend on N, and are inside data["phi"]
    '''
    for plot_quantity_all_index, plot_quantity in enumerate(plot_names.keys()):
        # Create empty array to store the quantity;
        # input_list = [[Transformation 1 phi_halfway],[Transformation 2 phi_halfway]]
        # plot_list = [[Transformation 1 disc error],[Transformation 2 disc error]]
        plot_list = []
        for i in range(number_of_transformations):
            plot_list.append([])

        for data_index, data in enumerate(all_data):
            # Find the index of M in list_of_M
            # Figure out where in plot_list the data should go
            for M_index, M in enumerate(list_of_M):
                if data["M_unscaled"] == M:
                    break
            if plot_quantity_all_index > len(plot_quantities_phi)-1:

                new_item = [data["N"], data[plot_quantity]]
                try:
                    #For the hydrogen simulation, there is no charge conservation
                    #in that case data["charge_conservation_1"] is an empty list
                    #That cannot be converted to a float
                    float(data[plot_quantity])
                except:
                    #If this fails, set it to nan.
                    new_item[1] = float('nan')
            else:
                new_item = [data["N"], data["phi"][plot_quantity]]
            plot_list[M_index].append(new_item)

        # Sort each of the arrays in plot_list based on N
        for i in range(number_of_transformations):
            plot_list[i] = sorted(plot_list[i], key=lambda wtf: wtf[0])
            # To check that this indeed sorts:
            # print(plot_list)

        # Plot the quantity of interest
        f = plt.figure()
        ax = plt.gca()
        for i in range(number_of_transformations):
            #Check if the current drawing is for a transformed, or untransformed
            if (np.eye(len(M)) == list_of_M[i]).all():
                plot_label = 'Original'
                plot_color = '#ff7f0e'
            else:
                plot_label = 'Transformed'
                plot_color = '#1f77b4'
            #Extract the x,y data from the nested lists
            x = []
            y = []
            for pair in plot_list[i]:
                x.append(pair[0])
                y.append(pair[1])
            #Make plot
            plt.plot(x, y, label=plot_label, color=plot_color)
            plt.xscale('log')
            if plot_quantity=="iterations":
                plt.yscale('linear')
            else:
                plt.yscale('log')
            plt.xlabel('Number of gridpoints')
            if plot_names[plot_quantity] in replacement_label:
                #labels.append('$'+label+'$')
                plt.ylabel('$'+replacement_label[plot_names[plot_quantity]]+'$', usetex=False)
            else:
                plt.ylabel(plot_names[plot_quantity], usetex=False)
            print(plot_names[plot_quantity])
        ax.legend(loc='best')
        ax.autoscale(enable=True, axis='x', tight=True)
        ax.autoscale(enable=True, axis='y', tight=False)
        for axis in ['top', 'bottom', 'left', 'right']:
                ax.spines[axis].set_linewidth(1.5)
        ax.tick_params(axis='both', direction='in', bottom=True,
                       top=True, left=True, right=True)
        f.savefig(folder+"/"+plot_quantity+".pdf", bbox_inches='tight')
        f.savefig(folder+"/"+plot_quantity+".png", bbox_inches='tight')
        plt.close(f)

    '''
    Discretization error plot
    This is done by comparing the value of phi in the center of the domain. This comparison is made
    between the computed value with highest N, and all others.

    Since phi is an array at each gridpoint, we have to somehow compare multiple values.
    In the inf-norm version, the worst error is taken, in the 1-norm version ||phi_exact-phi||_1/len(phi).
    This gives the "average error" over all components.
    '''
    # Create empty array to store the quantity;
    input_list = []
    for i in range(number_of_transformations):
        input_list.append([])

    x_halfway = None
    for data_index, data in enumerate(all_data):
        # Find the index of M in list_of_M
        # Figure out where in input_list the data should go
        for M_index, M in enumerate(list_of_M):
            if data["M_unscaled"] == M:
                break
        new_phi = [data["N"], data["phi_halfway"]]
        #Sanity check if x_halfway is always the same or not
        if x_halfway == None:
            x_halfway = data["x_halfway"]
        else:
            if abs(x_halfway-data["x_halfway"]) > 1e-15:
                cprint('x_halfway is not the same for all phi!',
                       'red', attrs=['bold'])
                raise ValueError('x_halfway is not the same for all phi!')
        print(data["x_halfway"])
        input_list[M_index].append(new_phi)

    # Sort each of the arrays in input_list based on N (the first thing in input_list)
    for i in range(number_of_transformations):
        input_list[i] = sorted(input_list[i], key=lambda wtf: wtf[0])
        # To check that this indeed sorts:
        # print(input_list[i])

    # Plot the quantity of interest
    f = plt.figure()
    ax = plt.gca()
    for i in range(number_of_transformations):
        #Check if the current drawing is for a transformed, or untransformed
        if (np.eye(len(M)) == list_of_M[i]).all():
            plot_label = 'Original'
            plot_color = '#ff7f0e'
        else:
            plot_label = 'Transformed'
            plot_color = '#1f77b4'
        #Extract the x,y data from the nested lists
        N = []
        disc_error_inf = []
        # For transformation i, we want the phi value
        phi_exact = np.array(input_list[i][-1][1][:])
        print(input_list[i][-1][1][:])
        for pair in input_list[i]:
            N.append(pair[0])
            phi = np.array(pair[1][:])
            disc_error_inf.append(np.linalg.norm(phi_exact-phi, ord=np.inf))

        #Print to visually check everything went ok
        print('Convergence order inf:')
        for ix in range(0, len(disc_error_inf[0:-2])):
            print('N: '+str(N[ix])+' e(N[ix])/e(N[ix+1]): ' +
                  str(disc_error_inf[ix]/disc_error_inf[ix+1]))
        cprint('Avg order: '+str((np.log(disc_error_inf[-2])-np.log(disc_error_inf[0]))/(
            np.log(N[-2])-np.log(N[0]))), 'green', attrs=['bold'])

        #Add a line to the plot
        plt.plot(N[0:-2], disc_error_inf[0:-2],
                 label=plot_label, color=plot_color)
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel('N')
    ax.legend(loc='best')
    ax.autoscale(enable=True, axis='both', tight=True)
    for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                   top=True, left=True, right=True)
    f.savefig(folder+"/"+"disc_error_inf"+".pdf", bbox_inches='tight')
    f.savefig(folder+"/"+"disc_error_inf"+".png", bbox_inches='tight')
    plt.close(f)

    # Plot the quantity of interest
    f = plt.figure()
    ax = plt.gca()
    for i in range(number_of_transformations):
        #Check if the current drawing is for a transformed, or untransformed
        if (np.eye(len(M)) == list_of_M[i]).all():
            plot_label = 'Original'
            plot_color = '#ff7f0e'
        else:
            plot_label = 'Transformed'
            plot_color = '#1f77b4'
        #Extract the x,y data from the nested lists
        N = []
        disc_error_1 = []
        phi_exact = np.array(input_list[i][-1][1][:])
        print(input_list[i][-1][1][:])
        for pair in input_list[i]:
            N.append(pair[0])
            phi = np.array(pair[1][:])
            disc_error_1.append(np.linalg.norm(
                phi_exact-phi, ord=1)/len(phi_exact))

        #Print to visually check everything went ok
        print('Convergence order 1:')
        for ix in range(0, len(disc_error_1[0:-2])):
            print('N: '+str(N[ix])+' e(N[ix])/e(N[ix+1]): ' +
                  str(disc_error_1[ix]/disc_error_1[ix+1]))
        cprint('Avg order: '+str((np.log(disc_error_1[-2])-np.log(disc_error_1[0]))/(
            np.log(N[-2])-np.log(N[0]))), 'green', attrs=['bold'])

        #Add a line to the plot
        plt.plot(N[0:-2], disc_error_1[0:-2],
                 label=plot_label, color=plot_color)
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel('N')
        plt.ylabel("disc_error_1")
    ax.legend(loc='best')
    ax.autoscale(enable=True, axis='both', tight=True)
    for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1.5)
    ax.tick_params(axis='both', direction='in', bottom=True,
                   top=True, left=True, right=True)
    f.savefig(folder+"/"+"disc_error_1"+".pdf", bbox_inches='tight')
    f.savefig(folder+"/"+"disc_error_1"+".png", bbox_inches='tight')
    plt.close(f)


if process_parallel:
    #Plot in all folders in parallel
    pool = Pool(processes=2*len(folders))
    pool.map_async(plot_position, folders)
    pool.map_async(plot_N, folders)
    pool.close()
    pool.join()
else:
    for folder in folders:
        print(folder)
        plot_position(folder)
        plot_N(folder)
