% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
%{
    Supplementary Matlab code used to obtain the results present in
    Multicomponent transport in plasmas; exploiting stoichiometry.

    To run a single input file:
    run_folders=false
    input_filename=name of input file (without.m)

    To run every input file in the "inputs" directory:
    run_folders=true
    directories={'cell','of','folder','names'}
    It is possible to run all input files in parallel by using parfor,
    however this may lead to inaccurate timings.

    Output is provided in input_struct, which is serialized to a .json
    file.
%}

clear all
%close all

addpath('stefan_maxwell')
addpath('.')
main_path=pwd;

run_folders=false;
input_folders={'inputs/'};
directories={'8species','argon'};
input_filename='ar_t_4';

if run_folders
    cd(main_path)
    simulation={};
    counter=1;
    %Figure out which input files to run from which folders
    for input_folder_ix=1:length(input_folders)
        input_folder=input_folders{input_folder_ix};
        for iter_directory=1:length(directories)
            cd(main_path)
            files = dir([input_folder,directories{iter_directory},'/*.m']);
            for file_index=1:length(files)
                simulations{counter}.input_folder=input_folder; %#ok<*SAGROW>
                simulations{counter}.directory=directories{iter_directory};
                simulations{counter}.file=files(file_index);
                counter=counter+1;
            end
        end
    end

    %parfor
    %Run all the input files in parallel
    for simulation_ix=1:length(simulations)
        cd(main_path)
        input_folder=simulations{simulation_ix}.input_folder;
        directory=simulations{simulation_ix}.directory;
        file=simulations{simulation_ix}.file;
        try
            cd(file.folder)
            input_struct=input_file([file.name(1:end-2)]);
            input_struct = run_input_file(input_struct);

            input_struct=[];
        catch e
            fprintf(1,'The identifier was:\n%s',e.identifier);
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            cd(main_path)
        end
    end


else
    %Run only a single inputfile
    input_struct=input_file(input_filename);
    input_struct=run_input_file(input_struct);
    cd(main_path)
    keep_running=false
    input_struct.phi.error
end


