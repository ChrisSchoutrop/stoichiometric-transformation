% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef mf < handle
    %{
    Matrix functions (mf)
    Helper class to provide library of static functions needed to compute
    the matrix functions for the discretization scheme.
    %}
    
    properties (Constant)
        
    end
    
    methods (Static)
        function out=B(z)
            %{
            Bernoulli function
            B(z)=z/(exp(z)-1)
            %}
            [V,L]=eig(z);
            diag_L=diag(L);
            for iter=1:length(L)
                d=diag_L(iter);
                if abs(d)>1e-7
                    diag_L(iter)=d/expm1(d);
                else
                    %Use series expansion to avoid division by 0
                    %B(z)~1 - z/2 + z^2/12 - z^4/720
                    %The quartic term is well below machine epsilon if
                    %z<1e-7
                    diag_L(iter)=polyval([1/12,-0.5,1],d);
                end
            end
            L=diag(diag_L);
            out=V*L/V;
        end
        function out=W(z)
            %{
            W function
            W(z)=(exp(z)-1-z)/(z*(exp(z)-1))
            %}
            [V,L]=eig(z);
            diag_L=diag(L);
            for iter=1:length(L)
                d=diag_L(iter);
                if abs(d)>1e-7
                    exmp1d=expm1(d);
                    if isinf(exmp1d)
                        %{
                        Use asymptotic value for W to avoid
                        inf/inf
                        W(z)=1/z-1/expm1(z)
                        %}
                        if d>0
                            diag_L(iter)=0+1./d;
                        else
                            diag_L(iter)=1+1./d;
                        end
                    else
                        %No weird exceptions occured, just apply formula
                        %for W(d)
                        diag_L(iter)=(expm1(d)-d)./(d.*expm1(d));
                    end
                else
                    %Use series expansion to avoid division by 0
                    %W(z)~1/2 - z/12 + z^3/720
                    %The cubic term is well below machine epsilon if
                    %z<1e-7
                    diag_L(iter)=polyval([720,0,-1/12,0.5],d);
                end
            end
            L=diag(diag_L);
            out=V*L/V;
        end
        function out=sgn(z)
            %{
            sgn function
            sgn(A)=V*sgn(L)*inv(V)
            with definition: sgn(0)=1
            %}
            [V,L]=eig(z);
            diag_L=diag(L);
            for iter=1:length(L)
                if diag_L(iter)>=0
                    diag_L(iter)=1;
                else
                    diag_L(iter)=-1;
                end
            end
            L=diag(diag_L);
            out=V*L/V;
        end
        function out=A(U,E)
            %out=E\U;
            %cond(E) can be large, cond(Es) usually much reduced.
            Qs=diag(1./sum(abs(E),2));
            Ps=diag(1./sum(abs(Qs*E),1)');
            Es=Qs*E*Ps;
            out=(Ps*(Es\Qs))*U;
        end
        function out=P(A,dx)
            out=dx*A;
        end
        function out=alpha(E,P,dx)
            %HF/CF
            out=E*mf.B(-P)./dx;
            %CD
            %U=E*P/dx;
            %out=0.5*U+E/dx;
        end
        function out=beta(E,P,dx)
            %HF/CF
            out=E*mf.B(+P)./dx;
            %CD
            %U=E*P/dx;
            %out=-0.5*U+E/dx;
        end
        function out=Q(P,E)
            %{
            Q=0.5*I-E*W*inv(E)
            %}
            %out=0.5.*eye(length(P))-mf.W(E*P/E);
            %out=0.5.*eye(length(P))-E*mf.W(P)/E;
            
            %cond(E) can be large, cond(Es) usually much reduced.
            Qs=diag(1./sum(abs(E),2));
            Ps=diag(1./sum(abs(Qs*E),1)');
            Es=Qs*E*Ps;
            
            out=0.5.*eye(length(P))-E*mf.W(P)*(Ps*(Es\Qs));
        end
        function out=Q2(U,E,dx)
            %{
            mat_Q = 0.5*np.identity(self.N_phi) - helper.W_fn_matrix(mat_Peclet_tilde)
            %}
            Ptilde=dx*U/E;
            out=0.5.*eye(length(U))-mf.W(Ptilde);
        end
        function out=sigma(E,A)
            %{
            sigma=E*sgn(A)*inv(E)
            %}
            %out=mf.sgn(E*A/E); %Strictly speaking this is only ok if sgn()
            %has a series expansion.
            %out=E*mf.sgn(A)/E;
            
            %cond(E) can be large, cond(Es) usually much reduced.
            Qs=diag(1./sum(abs(E),2));
            Ps=diag(1./sum(abs(Qs*E),1)');
            Es=Qs*E*Ps;
            out=E*mf.sgn(A)*(Ps*(Es\Qs));
        end
        function out=gamma(Q,P,sigma)
            out=0.5*Q*(eye(length(P))+sigma); %CF scheme
            %out=zeros(size(P)); %HF/CD schemes
        end
        function out=delta(Q,P,sigma)
            out=0.5*Q*(eye(length(P))-sigma); %CF Scheme
            %out=zeros(size(P)); %HF/CD schemes
        end
        function J=Jacobian(fn,x0)
            %{
            Computes the Jacobian using finite differences. Only used for
            Newton iterations if no Jacobian for the chemical source term
            is provided.
            %}
            N=length(x0);
            J=zeros(N,N);
            dx=abs(x0)*sqrt(eps);
            dx(dx==0)=sqrt(eps);
            %fn_x0=fn(x0); %Forward difference
            for i=1:N
                dx_vec=zeros(N,1);
                dx_vec(i)=dx(i);
                %J(:,i)=(fn(x0+dx_vec)-fn_x0)./dx(i);   %Forward difference
                J(:,i)=(fn(x0+dx_vec)-fn(x0-dx_vec))./(2.*dx(i));   %Central difference
            end
        end
        function out=empty_vector_cells(N,m)
            out=cell(N,1);
            for iter=1:N
                out{iter}=zeros(m,1);
            end
        end
        function out=empty_matrix_cells(N,m)
            out=cell(N,1);
            for iter=1:N
                out{iter}=zeros(m,m);
            end
        end
        function out=transform_matrix(mat,M)
            %Equivalent to M*mat*inv(M)
            %out=M*mat/M;
            %out=M*mat*invM;
            Qs=diag(1./sum(abs(M),2));
            Ps=diag(1./sum(abs(Qs*M),1)');
            Ms=Qs*M*Ps;
            out=M*mat*(Ps/Ms)*Qs;
        end
        function out=inv_transform_matrix(mat,M)
            %Equivalent to inv(M)*mat*M
            %out=M\mat*M;
            %out=invM*mat*M;
            Qs=diag(1./sum(abs(M),2));
            Ps=diag(1./sum(abs(Qs*M),1)');
            Ms=Qs*M*Ps;
            out=(Ps/Ms)*Qs*mat*M;
        end
        
        function out=transform_vector(vec,M)
            out=M*vec;
        end
        function out=inv_transform_vector(vec,M)
            %Equivalent to inv(M)*vec
            %out=M\vec;
            %out=invM*vec;
            Qs=diag(1./sum(abs(M),2));
            Ps=diag(1./sum(abs(Qs*M),1)');
            Ms=Qs*M*Ps;
            out=Ps*(Ms\(Qs*vec));
        end
        
        function out=g(z,h)
            %{
            Arbritary matrix function for function handle h
            %}
            [V,L]=eig(z);
            out=V*h(L)/V;
        end
        
    end
    
end

