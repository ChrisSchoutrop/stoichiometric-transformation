% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
%% Test matrix functions
x=zeros(5,5);
assert(all(all(mf.B(x)==eye(length(x)) )));
assert(all(all(mf.W(x)==eye(size(x))*0.5)));

for i=1:10
    N=randi([1,20]);
    x=rand(N,N);
    
    B=@(z) z/(expm1(z));
    W=@(z) (expm1(z)-z)/(z*(expm1(z)));
    
    assert(norm(mf.g(x,B)-mf.B(x),inf)<1e-12);
    assert(norm(mf.g(x,W)-mf.W(x),inf)<1e-12);
    
    
    x=rand().*eye(N);
    assert(all(all(mf.sgn(x)==eye(length(x))) ))
    x=zeros(N);
    assert(all(all(mf.sgn(x)==eye(length(x))) ))
    x=-rand().*eye(N);
    assert(all(all(mf.sgn(x)==-eye(length(x))) ))
    
    U=rand(N)-0.5;
    E=rand(N)-0.5;
    dx=rand();
    
    %A
    A_expected=inv(E)*U;
    assert(norm(A_expected-mf.A(U,E),inf)<1e-10);
    %P
    P_expected=dx*inv(E)*U;
    assert(norm(P_expected-mf.P(mf.A(U,E),dx),inf)<1e-10);
    %Q
    Q_expected=0.5*eye(N)-E*mf.W(P_expected)*inv(E);
    assert(norm(Q_expected-mf.Q(mf.P(mf.A(U,E),dx),E),inf)<1e-10);
    %sigma
    sigma_expected=E*mf.sgn(A_expected)*inv(E);
    assert(norm(sigma_expected-mf.sigma(E,mf.A(U,E)),inf)<1e-10);
    %alpha
    alpha_expected=E*mf.B(-mf.P(mf.A(U,E),dx))/dx;
    assert(norm(alpha_expected-mf.alpha(E,mf.P(mf.A(U,E),dx),dx),inf)<1e-10);
    %beta
    beta_expected=E*mf.B(+mf.P(mf.A(U,E),dx))/dx;
    assert(norm(beta_expected-mf.beta(E,mf.P(mf.A(U,E),dx),dx),inf)<1e-10);
    %gamma
    gamma_expected=0.5*Q_expected*(eye(N)+sigma_expected);
    assert(norm(gamma_expected-mf.gamma(mf.Q(mf.P(mf.A(U,E),dx),E),mf.P(mf.A(U,E),dx),mf.sigma(E,mf.A(U,E))),inf)<1e-10);
    %delta
    delta_expected=0.5*Q_expected*(eye(N)-sigma_expected);
    assert(norm(delta_expected-mf.delta(mf.Q(mf.P(mf.A(U,E),dx),E),mf.P(mf.A(U,E),dx),mf.sigma(E,mf.A(U,E))),inf)<1e-10);
    
end
