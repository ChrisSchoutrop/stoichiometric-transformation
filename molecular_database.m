% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function [ info ] = molecular_database( name )
switch name
    case 'Hg'
        info.charge=0;
        info.polarizability=[8.41837e-30 16.4175e-50 63.4324e-70];
        info.radius=2.09;
        info.mass=PhysConst.m_Hg;
        info.molecule=0;
        info.N=[2 78]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
        info.levels=[1  	  0.000000;
            1  	  4.6673826;
            3  	  4.8864943;
            5  	  5.4606244;
            3  	  6.70366190;
            3  	  7.7304546;
            1  	  7.9260762;
            5  	  8.540828;
            1  	  8.6189564;
            3  	  8.6369623;
            7  	  8.79447;
            5  	  8.8285790;
            3  	  8.839436;
            5  	  8.8441709;
            3  	  8.8445369;
            5  	  8.8519843;
            7  	  8.8563371];
        info.energy=0;
        info.spin_mult=1;
    case 'Hg[+1]'
        info.charge=1;
        info.polarizability=[5.2482e-30 7.7752e-50 20.9118e-70];
        info.radius=2.09;
        info.mass=PhysConst.m_Hg-PhysConst.m_e;
        info.molecule=0;
        info.N=[1 78]; %for atoms [Next Nint] electrons %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Hg'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[2  	  0.000000;
            6  	  4.4032519;
            4  	  6.2680911;
            2  	  6.3834587;
            4  	  7.5144787;
            6  	  9.8821924];
        info.energy= 10.43750;
        info.spin_mult=2;
    case 'Hg2'
        info.charge=0;
        info.polar=1.4844e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.58;
        info.mass=2*PhysConst.m_Hg;
        info.molecule=1;
        info.N=[2 2]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Hg'; B(1)=2;
        info.energy=-350*100*PhysConst.h*PhysConst.c/PhysConst.e; %binding energy ground state: 350 cm^-1
        info.levels=[1 18.5 0.0127 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 2]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %values from J. Chem. Phys. 88, 4650 (1988); %recheck term symbol
        info.spin_mult=1;
    case 'e[-]'
        info.charge=-1;
        info.polarizability=[0 0 0];
        info.radius=PhysConst.re/1e-10;
        info.mass=PhysConst.m_e;
        info.molecule=0;
        info.N=[0 0]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.levels=[2   0];
        info.energy=0;
        info.spin_mult=2;
        info.electron=1;
    case 'Na[-1]'
        info.charge=-1;
        info.polarizability=[1.7797e-29 81.3201e-50 767.4414e-70];
        info.radius=2.16;
        info.mass=PhysConst.m_Na+PhysConst.m_e;
        info.molecule=0;
        info.N=[2 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=1;
        info.levels=[1 0];
        info.energy=-1; %real value unknown
        info.spin_mult=1;
    case 'Na'
        info.charge=0;
        info.polarizability=[1.7797e-29 81.3201e-50 767.4414e-70];
        info.radius=2.16;
        info.mass=PhysConst.m_Na;
        info.molecule=0;
        info.N=[1 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
        info.levels=[2  	  0.00000000;
            2  	  2.102297049;
            4  	  2.104429074;
            2  	  3.1913529;
            6  	  3.6169706;
            4  	  3.6169768;
            2  	  3.752628;
            4  	  3.753321;
            2  	  4.1163586;
            6  	  4.2834962;
            4  	  4.2835006;
            8  	  4.288231;
            6  	  4.288231;
            2  	  4.344453;
            4  	  4.344759;
            2  	  4.5096296];
        info.energy=0;
        info.spin_mult=2;
    case 'Na[+1]'
        info.charge=1;
        info.polarizability=[0.1445e-30 0.0509e-50 0.0406e-70];
        info.radius=2.16;
        info.mass=PhysConst.m_Na-PhysConst.m_e;
        info.dependent=0;
        info.molecule=0;
        info.N=[8 2]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[1 0];
        info.energy=5.1390766;
        info.spin_mult=1;
    case 'Na2'
        info.charge=0;
        info.polar=3.0919e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7]; %fitted values
        info.radius=3.70;
        info.mass=2*PhysConst.m_Na;
        info.dependent=0;
        info.molecule=1;
        info.N=[2 0]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=2;
    case 'I[-1]'
        info.charge=-1;
        info.polarizability=[4.6689e-30 8.6631e-50 32.9941e-70];
        info.radius=1.86;
        info.mass=PhysConst.m_I+PhysConst.m_e;
        info.dependent=0;
        info.molecule=0;
        info.N=[8 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='I'; B(1)=1;
        info.dep_species{2}='e[-]';B(2)=1;
        info.levels=[1 0];
        info.energy=-3;
        info.spin_mult=1;
    case 'I'
        info.charge=0;
        info.polarizability=[4.6689e-30 8.6631e-50 32.9941e-70];
        info.radius=1.86;
        info.mass=PhysConst.m_I;
        info.molecule=0;
        info.N=[7 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
        info.levels=[4  	  0.000000;
            2  	  0.9426481];
        info.energy=0;
        info.spin_mult=2;
    case 'I[+1]'
        info.charge=1;
        info.polarizability=[2.7338e-30 3.6831e-50 9.3441e-70];
        info.radius=1.86;
        info.mass=PhysConst.m_I-PhysConst.m_e;
        info.molecule=0;
        info.N=[6 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='I'; B(1)=1;
        info.dep_species{2}='e[-]';B(2)=-1;
        info.levels=[5  	  0.0000;
            1  	  0.79944;
            3  	  0.8786621;
            5  	  1.70196;
            1  	  3.65769];
        info.energy=10.4513;
        info.levels=[5 0];
        info.spin_mult=3;
    case 'I2'
        info.charge=0;
        info.polar=10.619e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.39;
        info.mass=2*PhysConst.m_I;
        info.molecule=1;
        info.N=[2 12]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='I'; B(1)=2;
        info.energy=-1.56662;
        info.levels=[1 286 0.368 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 2]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %values from PLASIMO
        info.spin_mult=1; %nist webbook constants of diatomic molecules
    case 'NaI'
        info.charge=0;
        info.polar=2.2398e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7]; %fitted values Na
        info.radius=3.44;
        info.mass=PhysConst.m_Na+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 6]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='I'; B(2)=1;
        info.energy=-3.1611;
        info.levels=[1 258.7 6.62 (0)*PhysConst.e./100/PhysConst.h/PhysConst.c 1]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %J. Chem. Phys. 84, 5735 (1986); doi: 10.1063/1.449881
        info.spin_mult=1;
    case 'Na2I2'
        info.charge=0;
        info.polar=4.6017e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7]; %fitted values Na
        info.radius=4.81;
        info.mass=2*PhysConst.m_Na+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 12]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=2;
        info.dep_species{2}='I'; B(2)=2;
        info.energy=-8.13;
        info.levels=[1 258.7 6.62 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %sane as NaI
        info.spin_mult=1;
    case 'Na3I3'
        info.charge=0;
        info.polar=7.3652e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7]; %fitted values Na
        info.radius=5.2;
        info.mass=3*PhysConst.m_Na+3*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 18]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=3;
        info.dep_species{2}='I'; B(2)=3;
        info.energy=-12.65;
        info.levels=[1 258.7 6.62 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %same as NaI
        info.spin_mult=1;
    case 'HgI'
        info.charge=0;
        info.polar=1.2903e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.38;
        info.mass=PhysConst.m_Hg+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 7]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Hg'; B(1)=1;
        info.dep_species{2}='I'; B(2)=1;
    case 'HgI2'
        info.charge=0;
        info.polar=1.8711e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.15;
        info.mass=PhysConst.m_Hg+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 12]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Hg'; B(1)=1;
        info.dep_species{2}='I'; B(2)=2;
    case 'Hg2I2'
        info.charge=0;
        info.polar=2.4594e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.7;
        info.mass=2*PhysConst.m_Hg+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Hg'; B(1)=2;
        info.dep_species{2}='I'; B(2)=2;
    case 'Tl'
        info.charge=0;
        info.polarizability=[1.29973e-29 48.9580e-50 367.1178e-70];
        info.radius=2.07;
        info.mass=PhysConst.m_Tl;
        info.molecule=0;
        info.N=[3 78]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.levels=[2  	  0.0000;
            4  	  0.96617;
            2  	  3.28279;
            2  	  4.23529;
            4  	  4.35942;
            4  	  4.47805;
            6  	  4.48822;
            2  	  4.80388;
            2  	  5.12899;
            4  	  5.17520;
            4  	  5.20875;
            6  	  5.21341;
            14  	  5.24681;
            2  	  5.35193;
            2  	  5.50253;
            4  	  5.52505;
            4  	  5.53870;
            6  	  5.54119;
            14  	  5.55741;
            2  	  5.6066;
            2  	  5.61609];
        info.energy=0;
        info.spin_mult=2;
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
    case 'Tl[+1]'
        info.charge=1;
        info.polarizability=[4.5741e-30 6.0202e-50 14.4059e-70];
        info.radius=2.07;
        info.mass=PhysConst.m_Tl-PhysConst.m_e;
        info.molecule=0;
        info.N=[2 78]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.levels=[1  	  0.00000;
            1  	  6.131198;
            3  	  6.496023;
            5  	  7.653204;
            3  	  9.381057];
        info.energy=6.1082870;
        info.spin_mult=1;
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
    case 'Tl2'
        info.charge=0;
        info.polar=2.2113e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.55;
        info.mass=2*PhysConst.m_Tl;
        info.molecule=1;
        info.N=[2 4]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=2;
    case 'TlI'
        info.charge=0;
        info.polar=1.7195e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.37;
        info.mass=PhysConst.m_Tl+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 8]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.energy=-3.1715;
        info.levels=[1 152  1.5987 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1];%electronic weight omega(cm-1) B(cm-1) T(cm-1) %1986 J. Phys. B: At. Mol. Phys. 19 1291
        info.spin_mult=1;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=1;
        info.dep_species{2}='I'; B(2)=1;
    case 'TlI2'
        info.charge=0;
        info.polar=2.2488e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.14;
        info.mass=PhysConst.m_Tl+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 13]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.energy=-6.7264;
        info.levels=[1 152  1.5987 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1];%electronic weight omega(cm-1) B(cm-1) T(cm-1) %1986 J. Phys. B: At. Mol. Phys. 19 1291 %same as TlI
        info.spin_mult=1;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=1;
        info.dep_species{2}='I'; B(2)=2;
    case 'TlI3'
        info.charge=0;
        info.polar=2.6358e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.51;
        info.mass=PhysConst.m_Tl+3*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 18]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.energy=-9.2864;
        info.levels=[1 152  1.5987 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 3];%electronic weight omega(cm-1) B(cm-1) T(cm-1) %1986 J. Phys. B: At. Mol. Phys. 19 1291
        info.spin_mult=1;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=1;
        info.dep_species{2}='I';  B(2)=3;
    case 'Tl2I2'
        info.charge=0;
        info.polar=3.4245e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.68;
        info.mass=2*PhysConst.m_Tl+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 16]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Tl'; B(1)=2;
        info.dep_species{2}='I';  B(2)=2;
    case 'In'
        info.charge=0;
        info.polarizability=[1.25779e-29 50.2027e-50 403.1325e-70];
        info.radius=2.02;
        info.mass=PhysConst.m_In;
        info.molecule=0;
        info.N=[3 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
        info.levels=[2     0.000000;
            4     0.2743273;
            2     3.0218614;
            2     3.9448028;
            4     3.9817834;
            4     4.0781165;
            6     4.0810065;
            2  	  4.3366791;
            4  	  4.4660073;
            2  	  4.5008572;
            6  	  4.6434512;
            2  	  4.818203;
            4  	  4.832023;
            4  	  4.841400;
            6  	  4.847581;
            8	  4.923113;
            6  	  4.923113;
            2  	  5.038343];
        info.energy=0;
        info.spin_mult=2;
    case 'In[+1]'
        info.charge=1;
        info.polarizability=[4.2866e-30 6.1156e-50 16.1485e-70];
        info.radius=2.02;
        info.mass=PhysConst.m_In-PhysConst.m_e;
        info.molecule=0;
        info.N=[2 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[1  	  0.00;
            1  	  5.2414;
            1  	  5.3746;
            5  	  5.6818;
            1  	  7.815196;
            1  	  11.64447];
        info.energy=5.7863551;
        info.spin_mult=1;
    case 'In2'
        info.charge=0;
        info.polar=2.126e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.46;
        info.mass=2*PhysConst.m_In;
        info.molecule=1;
        info.N=[2 4]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=2;
    case 'InI'
        info.charge=0;
        info.polar=1.6701e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.32;
        info.mass=PhysConst.m_In+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 8]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=1;
        info.dep_species{2}='I';  B(2)=1;
        info.energy=-3.1292; %discretization of curves from Theoretical Spectroscopic Studies of InI and InI+
        info.levels=[1 166 0.0359 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1];
        info.spin_mult=1;
    case 'InI2'
        info.charge=0;
        info.polar=2.1885e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.1;
        info.mass=PhysConst.m_In+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 13]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=1;
        info.dep_species{2}='I';  B(2)=2;
        info.energy=-5.1199;
        info.levels=[1 166 0.0359 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1]; %electronic weight omega(cm-1) B(cm-1)Te(cm-1) %values from InI
        info.spin_mult=1; %values from InI
    case 'In2I2'
        info.charge=0;
        info.polar=3.155e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.59;
        info.mass=2*PhysConst.m_In+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 16]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=2;
        info.dep_species{2}='I';  B(2)=2;
    case 'InI3'
        info.charge=0;
        info.polar=2.5711e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.47;
        info.mass=PhysConst.m_In+3*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 18]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=1;
        info.dep_species{2}='I' ; B(2)=3;
        info.energy=-6.9129;
        info.levels=[1 166 0.0359 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 3]; %electronic weight omega(cm-1) B(cm-1) T(cm-1) %values from InI
        info.spin_mult=1; %values form InI
    case 'In2I4'
        info.charge=0;
        info.polar=3.8659e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.97;
        info.mass=2*PhysConst.m_In+4*PhysConst.m_I;
        info.molecule=1;
        info.N=[10 24]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=2;
        info.dep_species{2}='I' ; B(2)=4;
    case 'In2I6'
        info.charge=0;
        info.polar=5.0571e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=5.35; %value estimated: radius In2I4+(radius In2I4- radius In2I2)
        info.mass=2*PhysConst.m_In+6*PhysConst.m_I;
        info.molecule=1;
        info.N=[12 36]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=2;
        info.dep_species{2}='I';  B(2)=6;
    case 'Xe'
        info.charge=0;
        info.polarizability=[3.6764e-30 5.6551e-50 17.8912e-70];
        info.radius=2.28;
        info.mass=PhysConst.m_Xe;
        info.molecule=0;
        info.N=[8 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1; %is a collision integral with electrons specified?
        info.levels=[1 0];
        info.energy=0;
        info.spin_mult=1;
    case 'Xe[+1]'
        info.charge=1;
        info.polarizability=[2.3070e-30 2.7010e-50 6.0040e-70];
        info.radius=2.28;
        info.mass=PhysConst.m_Xe-PhysConst.m_e;
        info.molecule=0;
        info.N=[7 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Xe'; B(1)=1;
        info.dep_species{2}='e[-]';B(2)=-1;
        info.levels=[4  	  0.00000;
            2  	  1.306423];
        info.energy=12.12;
        info.spin_mult=2;
    case 'Xe2'
        info.charge=0;
        info.polar=7.8840e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.14;
        info.mass=2*PhysConst.m_Xe;
        info.molecule=1;
        info.N=[2 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Xe'; B(1)=2;
    case 'NaXe'
        info.charge=0;
        info.polar=2.2287e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.3170*PhysConst.a0^7];
        info.radius=3.43;
        info.mass=PhysConst.m_Na+PhysConst.m_Xe;
        info.molecule=1;
        info.N=[2 7]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='Xe'; B(2)=1;
    case 'NaInI2'
        info.charge=0;
        info.polar=3.5836e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7];
        info.radius=4.69; %value estimated: radius NaTlI2 +radius In -radius Tl
        info.mass=PhysConst.m_Na+PhysConst.m_In+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 12]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='In'; B(2)=1;
        info.dep_species{3}='I';  B(3)=2;
    case 'NaTlI2'
        info.charge=0;
        info.polar=3.6352e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.5832*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.317*PhysConst.a0^7];
        info.radius=4.74;
        info.mass=PhysConst.m_Na+PhysConst.m_Tl+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 12]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Na'; B(1)=1;
        info.dep_species{2}='Tl'; B(2)=1;
        info.dep_species{3}='I';  B(3)=2;
    case 'InTlI2'
        info.charge=0;
        info.polar=3.1225e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.83;%value estimated: radius NaTlI2 +radius In -radius Na
        info.mass=PhysConst.m_In+PhysConst.m_Tl+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='In'; B(1)=1;
        info.dep_species{2}='Tl'; B(2)=1;
        info.dep_species{3}='I';  B(3)=2;
    case 'Ce'
        info.charge=0;
        info.polarizability=[2.3011e-29 100.8203e-50 867.8385e-70];
        info.radius=2.31;
        info.mass=PhysConst.m_Ce;
        info.molecule=0;
        info.N=[4 54]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0; %is a collision integral with electrons specified?
    case 'Ce[+1]'
        info.charge=1;
        info.polarizability=[3.8228e-30 9.7916e-50 46.8191e-70];
        info.radius=4.74;
        info.mass=PhysConst.m_Ce-PhysConst.m_e;
        info.molecule=0;
        info.N=[3 54]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=1;
        info.dep_species{2}='e-'; B(2)=-1;
    case 'Ce2'
        info.charge=0;
        info.polar=4.4108e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.96;
        info.mass=PhysConst.m_Ce*2;
        info.molecule=1;
        info.N=[2 6]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=2;
    case 'CeI'
        info.charge=0;
        info.polar=2.9551e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.58;
        info.mass=PhysConst.m_Ce+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 9]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=1;
        info.dep_species{2}='I'; B(2)=1;
    case 'CeI2'
        info.charge=0;
        info.polar=3.674e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.31;
        info.mass=PhysConst.m_Ce+PhysConst.m_I*2;
        info.molecule=1;
        info.N=[4 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=1;
        info.dep_species{2}='I'; B(2)=2;
    case 'CeI3'
        info.charge=0;
        info.polar=4.2042e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.70;
        info.mass=PhysConst.m_Ce+PhysConst.m_I*3;
        info.molecule=1;
        info.N=[6 19]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=1;
        info.dep_species{2}='I'; B(2)=3;
    case 'Ce2I6'
        info.charge=0;
        info.polar=8.053e-29;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=5.65;
        info.mass=PhysConst.m_Ce*2+PhysConst.m_I*6;
        info.molecule=1;
        info.N=[14 36]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0; %is a collision integral with electrons specified?
        info.dep_species{1}='Ce'; B(1)=2;
        info.dep_species{2}='I'; B(2)=6;
    case 'Al'
        info.charge=0;
        info.polarizability=[9.76*PhysConst.a0^3 36.8*PhysConst.a0^5 303.5*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=26.9815384*PhysConst.AMU;
        info.molecule=0;
        info.N=[8 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1;
        info.levels=[1 0.0000];
        info.energy=0;
        info.spin_mult=1;
    case 'Al[+1]'
        info.charge=1;
        info.polarizability=[5.402*PhysConst.a0^3 14.69*PhysConst.a0^5 82.55*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=26.9815384*PhysConst.AMU-PhysConst.m_e;
        info.molecule=0;
        info.N=[7 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Al'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[6 0];
        info.energy=20;%15.7596109;
        info.spin_mult=2;
    case 'Ar'
        info.charge=0;
        info.polarizability=[9.76*PhysConst.a0^3 36.8*PhysConst.a0^5 303.5*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=39.95*PhysConst.AMU;
        info.molecule=0;
        info.N=[8 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1;
        info.levels=[1 0.0000];
        info.energy=0;
        info.spin_mult=1;
    case 'Ar[*]'
        %{
            Assumed same as Ar
        %}
        info.charge=0;
        info.polarizability=[9.76*PhysConst.a0^3 36.8*PhysConst.a0^5 303.5*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=39.95*PhysConst.AMU;
        info.molecule=0;
        info.N=[8 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=1;
        info.levels=[1 0.0000];
        info.energy=0;
        info.spin_mult=1;
    case 'Ar[+1]'
        info.charge=1;
        info.polarizability=[5.402*PhysConst.a0^3 14.69*PhysConst.a0^5 82.55*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=39.95*PhysConst.AMU-PhysConst.m_e;
        info.molecule=0;
        info.N=[7 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Ar'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[6 0];
        info.energy=20;%15.7596109;
        info.spin_mult=2;
    case 'Ar[+2]'
        info.charge=2;
        info.polarizability=[3.303*PhysConst.a0^3 6.918*PhysConst.a0^5 28.51*PhysConst.a0^7];
        info.radius=1.75;
        info.mass=39.95*PhysConst.AMU-2*PhysConst.m_e;
        info.molecule=0;
        info.N=[6 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Ar'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-2;
        info.levels=[6 0];
        info.energy=27.62967+15.7596109;
        info.spin_mult=3;
    case 'Ar2'
        info.charge=0;
        info.polar=3.0585e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=2.35;
        info.mass=79.90*PhysConst.AMU;
        info.molecule=1;
        info.N=[2 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Ar'; B(1)=2;
        info.levels=[];
        info.energy=-0.0104;
        info.spin_mult=1; %Constants of diatomic molecules nist
    case 'Ar2[+1]'
        %{
            Adapted from Ar2
        %}
        info.charge=1;
        info.polar=3.0585e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=2.35;
        info.mass=2*79.90*PhysConst.AMU;
        info.molecule=1;
        info.N=[2 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Ar'; B(1)=2;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[];
        info.energy=-0.0104;
        info.spin_mult=1; %Constants of diatomic molecules nist
    case 'S'
        info.charge=0;
        info.polarizability=[17.13*PhysConst.a0^3 105.5*PhysConst.a0^5 1420*PhysConst.a0^7];
        info.radius=1.43;
        info.mass=32.06*PhysConst.AMU;
        info.molecule=0;
        info.N=[6 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[5  	  0.000000;
            3  	  0.0491046;
            1  	  0.0711223;
            5  	  1.145441;
            1  	  2.7499637];
        info.energy=0;
        info.spin_mult=3;
    case 'S[+1]'
        info.charge=1;
        info.polarizability=[6.343*PhysConst.a0^3 20.89*PhysConst.a0^5 143*PhysConst.a0^7];
        info.radius=1.43;
        info.mass=32.06*PhysConst.AMU-PhysConst.m_e;
        info.molecule=0;
        info.N=[5 10]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.dep_species{1}='S'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.coll_int=0;
        info.levels=[4 0];
        info.energy=10.36;
        info.spin_mult=4;
    case 'S2'
        info.charge=0;
        info.polar=4.7134e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=2.45;
        info.mass=64.12*PhysConst.AMU;
        info.molecule=1;
        info.N=[4 8]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='S'; B(1)=2;
        info.energy=-4.088;
        info.levels=[3 2.844 0.29541 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 2;
            3 2.75 0.2244 31907 2]; %electronic weight omega(cm-1) B(cm-1)Te(cm-1) %values from InI
        info.spin_mult=3; %Constants of diatomic molecules nist
    case 'H'
        info.charge=0;
        info.polar=0.667e-30; %J. Chem. Phys. 46, 1426 (1967);
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^5.6733*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^15.9029*PhysConst.a0^7]; %used powers from He
        info.radius=0.53;
        info.mass=PhysConst.m_H;
        info.molecule=0;
        info.N=[0 1]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[2  	  0];
        info.energy=0;
        info.spin_mult=2; %Constants of diatomic molecules nist
    case 'H2'
        info.charge=0;
        info.polar=0.8056e-30; %J. Chem. Phys. 46, 1426 (1967);
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^5.6733*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^15.9029*PhysConst.a0^7]; %used powers from He
        info.radius=0.73;
        info.mass=2*PhysConst.m_H;
        info.molecule=1;
        info.N=[2 1]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.dep_species{1}='H'; B(1)=2;
        info.coll_int=0;  %?
        info.levels=[2 0]; %?
        info.energy=0;
        info.spin_mult=2; %Constants of diatomic molecules nist
        % Guessed some values N_ext
    case 'H[+1]'
        info.charge=1;
        info.polar=1.20e-48;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^5.6733*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^15.9029*PhysConst.a0^7]; %used powers from He
        info.radius=0.8775e-15;
        info.mass=PhysConst.m_H-PhysConst.m_e;
        info.molecule=0;
        info.N=[0 0]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='H'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[1  	  0];
        info.energy=20;%13.598433770784;
        info.spin_mult=1; %Constants of diatomic molecules nist
    case 'He'
        info.charge=0;
        info.polarizability=[1.166.*PhysConst.a0.^3    2.39*PhysConst.a0^5 11.5*PhysConst.a0^7];
        info.radius=1.04;
        info.mass=4.00*PhysConst.AMU;
        info.molecule=0;
        info.N=[0 2]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[1  	  0];
        info.energy=0;
        info.spin_mult=1; %Constants of diatomic molecules nist
    case 'N'
        info.charge=0;
        info.polarizability=[5.43.*PhysConst.a0.^3    21.2*PhysConst.a0^5 189.1*PhysConst.a0^7];
        info.radius=1.05;
        info.mass=14.01*PhysConst.AMU;
        info.molecule=0;
        info.N=[2 5]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[4  	  0.000000;
            6  	  2.3835296;
            4  	  2.3846099;
            2  	  3.5755702;
            4  	  3.5756180];
        info.energy=0;
        info.spin_mult=4; %Constants of diatomic molecules nist
    case 'N[+1]'
        info.charge=1;
        info.polarizability=[2.056.*PhysConst.a0.^3    4.671*PhysConst.a0^5 23.67*PhysConst.a0^7];
        info.radius=1.05;
        info.mass=14.01*PhysConst.AMU-PhysConst.m_e;
        info.molecule=0;
        info.N=[2 4]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='N'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[0  	  0.0000;
            1  	  0.00604;
            2  	  0.01622;
            2  	  1.89897;
            0  	  4.05289;
            2  	  5.80055];
        info.energy= 14.53413;
        info.spin_mult=3; %Constants of diatomic molecules nist
    case 'N2'
        info.charge=0;
        info.polar=1.3567.*PhysConst.a0.^3;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=1.80;
        info.mass=28.02*PhysConst.AMU;
        info.molecule=1;
        info.N=[6 8]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='N'; B(1)=2;
        info.levels=[1 1960 1.8793 0 2]; %electronic weight omega(cm-1) B(cm-1)Te(cm-1) %values from InI %the spectrum of molecular nitrogen p82/83
        info.energy=-9.6;
        info.spin_mult=1; %Constants of diatomic molecules nist
    case 'Sn'
        info.charge=0;
        info.polarizability=[56.05.*PhysConst.a0.^3    582.7*PhysConst.a0^5 12300*PhysConst.a0^7];
        info.radius=1.97;
        info.mass=PhysConst.m_Sn;
        info.molecule=0;
        info.N=[4 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[1  	  0.000000;
            3  	  0.2097572;
            5  	  0.4249773;
            5  	  1.067870;
            1  	  2.1278786;
            1  	  4.2949064;
            3  	  4.3288190;
            5  	  4.7893699;
            3  	  4.8672540;
            5  	  4.9129363;
            3  	  5.2497654;
            3  	  5.3770250;
            5  	  5.3846501;
            5  	  5.4159688;
            1  	  5.4303941;
            5  	  5.4732037;
            3  	  5.5183723;
            3  	  5.5267200];
        info.energy=0;
        info.spin_mult=3;
    case 'Sn[+1]'
        info.charge=1;
        info.polarizability=[28.12.*PhysConst.a0.^3    194.7*PhysConst.a0^5 2490*PhysConst.a0^7];
        info.radius=1.97;
        info.mass=PhysConst.m_Sn-PhysConst.m_e;
        info.molecule=0;
        info.N=[3 46]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.levels=[2  	  0.0000;
            4  	  0.52711;
            2  	  5.76083;
            4  	  5.99687;
            6  	  6.28972];
        info.energy=7.343917;
        info.spin_mult=2;
        info.dep_species{1}='Sn'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
    case 'SnI'
        info.charge=0;
        info.polar=13.147e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=3.29;
        info.mass=PhysConst.m_Sn+PhysConst.m_I;
        info.molecule=1;
        info.N=[2 9]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.levels=[1 199.0 0.0377 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1;
            1 129.8 0.0201 17916.7 1;
            1 241.1 0.0201 32172.8 1]; %electronic weight omega(cm-1) B(cm-1) Te(cm-1) %see NIST Constants of diatomic molecules
        info.energy=-1.94;
        info.spin_mult=2;
        info.dep_species{1}='Sn'; B(1)=1;
        info.dep_species{2}='I'; B(2)=1;
    case 'SnI2'
        info.charge=0;
        info.polar=19.e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.07;
        info.mass=PhysConst.m_Sn+2*PhysConst.m_I;
        info.molecule=1;
        info.N=[4 14]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.levels=[1 199.0 0.0377 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 1;
            1 129.8 0.0201 17916.7 1;
            1 241.1 0.0201 32172.8 1]; %electronic weight omega(cm-1) B(cm-1) Te(cm-1) %see NIST Constants of diatomic molecules %values from SnI
        info.energy=-5.42;
        info.spin_mult=1;
        info.dep_species{1}='Sn'; B(1)=1;
        info.dep_species{2}='I'; B(2)=2;
    case 'SnI3'
        info.charge=0;
        info.polar=23.663e-30;
        info.polarizability=[info.polar (info.polar/PhysConst.a0^3)^1.553*PhysConst.a0^5 (info.polar/PhysConst.a0^3)^2.238*PhysConst.a0^7];
        info.radius=4.44;
        info.mass=PhysConst.m_Sn+3*PhysConst.m_I;
        info.molecule=1;
        info.N=[6 19]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.levels=[1 199.0 0.0377 (0)*PhysConst.e/100/PhysConst.h/PhysConst.c 3;
            1 129.8 0.0201 17916.7 3;
            1 241.1 0.0201 32172.8 3]; %electronic weight omega(cm-1) B(cm-1) Te(cm-1) %see NIST Constants of diatomic molecules %values from SnI
        info.energy=-6.5295;
        info.spin_mult=2;
        info.dep_species{1}='Sn'; B(1)=1;
        info.dep_species{2}='I'; B(2)=3;
    case 'Ne'
        info.charge=0;
        info.polarizability=[2.92*PhysConst.a0^3 6.98*PhysConst.a0^5 38.9*PhysConst.a0^7];
        info.radius=1.22;
        info.mass=20.00*PhysConst.AMU;
        info.molecule=0;
        info.N=[8 2]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=1;
        info.coll_int=0;
        info.levels=[1 0.0000];
        info.energy=0;
        info.spin_mult=1;
    case 'Ne[+1]'
        info.charge=1;
        info.polarizability=[1.268*PhysConst.a0^3 1.913*PhysConst.a0^5 6.494*PhysConst.a0^7];
        info.radius=1.22;
        info.mass=20.00*PhysConst.AMU-PhysConst.m_e;
        info.molecule=0;
        info.N=[7 2]; %for atoms [Next Nint] %for molecules [bonding non-bonding] valence electrons
        info.dependent=0;
        info.coll_int=0;
        info.dep_species{1}='Ne'; B(1)=1;
        info.dep_species{2}='e[-]'; B(2)=-1;
        info.levels=[4 0
            2 0.09676024];
        info.energy=21.564539;
        info.spin_mult=2;
    case 'e[-1]'
        info.charge=-1;
        info.polarizability=0;
        info.radius=PhysConst.r_e*1e10; %Convert from [m] to [Angstrom]
        info.mass=PhysConst.m_e;
        info.molecule=0;
        info.N=[0 0];
        info.dependent=0;
        info.coll_int=0;
        info.levels=[];
        info.energy=0;
        info.spin_mult=2;
        
end
info.label=name;
info.radius=info.radius/1e10;           %Convert from [Angstrom] to [m]
info.energy=info.energy*PhysConst.eV;    %Convert from [eV] to [J]
end
