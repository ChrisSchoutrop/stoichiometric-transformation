% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function regularized_matrix = regularize(singular_matrix,vector)
%{
    Attempt to (quickly) find the constant that minimizes the condition
    number of a properly scaled version of the regularized matrix
    regularized_matrix=singular_matrix+constant*vector*vector'.
    In practice it seems that increasing the number of iterations beyond 3
    has no significant effect.
%}
%Lower bound of the search
c_min=0;

%Upper bound of the search
%Estimate the 'size' of the matrix and the vector.
c_max=norm(abs(singular_matrix))./norm(abs(vector));

dyadic_addition=(vector*vector');

options = optimset('Display','None','MaxFunEvals',3);
%options = optimset('Display','iter','MaxFunEvals',10);
constant=fminbnd(@(x)reg_internal(singular_matrix,dyadic_addition,x),c_min,c_max,options);

%constant=1;
regularized_matrix=singular_matrix+constant*dyadic_addition;
return;
end

function cond_val=reg_internal(singular_matrix,dyadic_addition,constant)
%Returns the condition number
cond_val=cond(reg_internal2(singular_matrix,dyadic_addition,constant));
return;
end

function regularized_matrix=reg_internal2(singular_matrix,dyadic_addition,constant)
%{
    Returns a regularized matrix, which has rows and columns scaled. This
    scaling is to eliminate trivial possible reductions in the condition
    number.
%}
regularized_matrix=singular_matrix+constant*dyadic_addition;

%Scale with rows and columns
Q=diag(1./sum(abs(regularized_matrix),2));
P=diag(1./sum(abs(Q*regularized_matrix),1));
regularized_matrix=P*inv(Q*regularized_matrix*P)*Q; %#ok<MINV>
return;
end
