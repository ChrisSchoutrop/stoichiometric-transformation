% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function input_struct = run_input_file(input_struct)
%{
Runs the input file object until completion
output is saved in the input_struct
%}
%Main object that contains everything about this computation
T=tcf_system(input_struct);

%Loop setup
starting_time=posixtime(datetime('now'));
plotcounter=0;
iter=0;
max_iter=1000;

%Main loop
while T.t<T.tmax && T.continue_stepping==true && iter<max_iter
    iter=iter+1
    if T.do_Newton
        T.advancetimestep_Newton();
    else
        T.advancetimestep_Picard();
    end
    if plotcounter==1000
        T.tcf_grid.plot_phi(T.t,input_struct);
        drawnow;
        plotcounter=0
    end
    plotcounter=plotcounter+1
end
input_struct=T.input_struct;

%{
Post processing
%}
input_struct.total_time=posixtime(datetime('now'))-starting_time;
T.tcf_grid.plot_phi(T.t,input_struct);

%{
Output to screen
%}
disp(['Mass Flux:',num2str(T.tcf_grid.c_U{1}(1,1))])
disp(['Pressure :',num2str(input_struct.pressure)])
disp(['Diff mat halfway:'])
E_halfway=T.tcf_grid.c_E{round(input_struct.N/2)}
disp(['phi halfway:'])
T.tcf_grid.c_phi{round(input_struct.N/2)}
Q=diag(1./sum(abs(E_halfway),2));
P=diag(1./sum(abs(Q*E_halfway),1));
E_halfway_scaled=Q*E_halfway*P

if isempty(input_struct.T_e)==false
    disp(['T_e :',num2str(input_struct.T_e)])
    disp(['T_i :',num2str(input_struct.T_i)])
end
disp('Rref of [E21 E22]')
rref(T.tcf_grid.c_E{round(input_struct.N/2)}(end-input_struct.Nelements+1:end,:))
disp('Condition number M')
cond(input_struct.M)

%Add more output quantities to the input_struct
input_struct.E_halfway=input_struct.M\T.tcf_grid.c_E{round(input_struct.N/2)}*input_struct.M;
input_struct.E_halfway_transformed=T.tcf_grid.c_E{round(input_struct.N/2)};
input_struct.phi_halfway=input_struct.M\T.tcf_grid.c_phi{round(input_struct.N/2)};
input_struct.phi_BC_west=input_struct.M\T.tcf_grid.phi_BC_west;
input_struct.phi_BC_east=input_struct.M\T.tcf_grid.phi_BC_east;
input_struct.s_fn_J=T.tcf_grid.s_fn_J;
input_struct.M\T.tcf_grid.c_phi{round(input_struct.N/2)}
input_struct.x_halfway=T.tcf_grid.vec_x(round(input_struct.N/2))
input_struct.mass_flux=T.tcf_grid.c_U{1}(1,1);

%Serialize the input_struct into a json object
timestamp=num2str(posixtime(datetime('now')) * 1e6);
input_struct_to_json(input_struct,['output_',timestamp,'.json'])



end

