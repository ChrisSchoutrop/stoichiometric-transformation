% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef stefan_maxwell
    %{
    Every static method related to Stefan-Maxwell
    %}
    properties
    end
    
    methods (Static)
        function charge_density=density_to_charge_density(densities,charges)
            charge_density=densities.*charges;
            return
        end
        function mass_frac=density_to_mass_frac(densities,masses)
            mass_frac=masses.*densities./sum(densities.*masses);
            return
        end
        function mol_frac=density_to_mol_frac(densities)
            mol_frac=densities./sum(densities);
            return
        end
        function mol_frac=mass_frac_to_mol_frac(mass_fraction,particleList)
            mol_frac=NaN(size(mass_fraction));
            
            mixture_mass=0;
            for iter=1:length(particleList)
                mixture_mass=mixture_mass+mass_fraction(iter)./particleList{iter}.mass;
            end
            mixture_mass=1./mixture_mass;
            for iter=1:length(particleList)
                mol_frac(iter)=mixture_mass.*mass_fraction(iter)./particleList{iter}.mass;
            end
            return
        end
        function densities=mass_frac_to_density(mass_fraction,particleList,pressure)
            %{
            Assumes ideal gas law
            %}
            NrParticles=length(particleList);
            sum2 = 0;
            for i = 1:NrParticles
                sum2 = sum2 + mass_fraction(i)*particleList{i}.temperature/particleList{i}.mass;
            end
            rho = pressure./(PhysConst.k_B*sum2); %=mn
            densities=zeros(NrParticles,1);
            for i = 1:NrParticles
                densities(i)=mass_fraction(i).*rho./particleList{i}.mass;
            end
            return
        end
        function rho=rho(densities,particleList)
            rho=0;
            for iter=1:length(particleList)
                rho=rho+densities(iter).*particleList{iter}.mass;
            end
            return
        end
        function E=diff_matrix(mass_frac,pressure,particleList,temperature)
            %{
            1. Calc: mol_frac, rho, densities from the mass fractions and
            pressure
            2. Pass these to the script from Lei
            3. Return the diffusion matrix E
            %}
            if sum(mass_frac)>1.01 || any(mass_frac>1.01) || any(mass_frac<-0.01)
                
                disp('Mass fractions:')
                disp(mass_frac)
                error('Mass fractions invalid')
            end
            if nargin==3
                %{
                This assumes ideal gas law
                %}
                densities=stefan_maxwell.mass_frac_to_density(mass_frac,particleList,pressure);
                
                rho=stefan_maxwell.rho(densities,particleList);
                mol_frac=stefan_maxwell.density_to_mol_frac(densities);
                for iter=1:length(particleList)
                    particleList{iter}.density=densities(iter);
                end
                
                E=diff_matrix_single(mass_frac,pressure,mol_frac,rho,densities,particleList);
                return
            elseif nargin==4
                %{
                Non-constant temperature only works if T_e=T_H
                %}
                N_particles=length(particleList);
                
                %Set the temperature in the particle list for later use
                for iter=1:N_particles
                    particleList{iter}.temperature=temperature;
                end
                
                densities=stefan_maxwell.mass_frac_to_density(mass_frac,particleList,pressure);
                rho=stefan_maxwell.rho(densities,particleList);
                mol_frac=stefan_maxwell.mass_frac_to_mol_frac(mass_frac,particleList);
                
                for iter=1:length(particleList)
                    particleList{iter}.density=densities(iter);
                end
                %mol_frac=densities./norm(densities,1);
                E=diff_matrix_single(mass_frac,pressure,mol_frac,rho,densities,particleList);
            end
        end
    end
    
end





















