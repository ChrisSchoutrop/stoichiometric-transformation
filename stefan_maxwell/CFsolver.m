% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function MassFractionMatrix = ...
    CFsolver(N,  NrParticles, dltx, particleList_old, Src, Gamma_ew, U_ew, bnd_l, bnd_r, urf, Schm)

% This function sovles systems of convetion-diffusion-reaction equations
% by the Exp scheme or the CF scheme

% solution matrix
MassFractionMatrix = zeros(N, NrParticles);

% system matrix
AA = zeros(NrParticles*N,NrParticles*N);
% if Schm == 1
%     BB = zeros(NrParticles*N,NrParticles*N);
% end

% the source vector
bb = zeros(NrParticles*N,1);

A_w = Gamma_ew(:,:,1)\U_ew(:,:,1);
%  eigenvalues/vectors of A, etc
[ V,Lambda ] = eig( A_w );
% if Schm == 1
%     S = V*sign(Lambda)/V; 
%     %S = signm( A_w ); % this function only valid for singular matrix
%     Sigma = Gamma_ew(:,:,1)*S/Gamma_ew(:,:,1);
% end

%  Bernoulli coefficient matrices, etc.
lambda = diag(Lambda); 
mu = dltx*lambda; 
diagvec = scBernoulli(mu); 
Bplus = V*diag(diagvec)/V;
diagminvec = scBernoulli(-mu); 
Bmin = V*diag(diagminvec)/V;

% if Schm ==1
%     Wplus_w = V*diag( [scWeightf(mu1) scWeightf(mu2) scWeightf(mu3)] )/V;  
%     Q = 0.5*eye(NrParticles) - Gamma_ew(:,:,1)*Wplus_w/Gamma_ew(:,:,1);
%     BW_w = 0.5*Q*( eye(NrParticles) + Sigma ); BE_w = 0.5*Q*( eye(NrParticles) - Sigma );
% end
%  matrix coefficients corresponding to homogeneous flux
AW_w = Gamma_ew(:,:,1)*Bmin/dltx; AE_w = Gamma_ew(:,:,1)*Bplus/dltx;

for k = 1:N-2
    % fill the contribution of the flux on the left interface of the
    % control volume to the system matrix
    
    %  computation coefficient matrix
    AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k-(NrParticles-1):NrParticles*k) = AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k-(NrParticles-1):NrParticles*k) - AW_w;
    AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) = AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) + AE_w/urf;
    
%     if Schm ==1
%         BB(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k-(NrParticles-1):NrParticles*k) = BB(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k-(NrParticles-1):NrParticles*k) + BW_w;
%         BB(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) = BB(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) + BE_w + eye(NrParticles);
%     end
    
    solutionVector = zeros(NrParticles,1);
    for i = 1:NrParticles
        solutionVector(i) = particleList_old(i).MassFraction(k+1);
    end
    % under relaxation
    bb(NrParticles*k+1:NrParticles*k+NrParticles) = bb(NrParticles*k+1:NrParticles*k+NrParticles) + (1 - urf)/urf*AE_w*solutionVector;
    
    
    % fill the contribution of the flux on the right interface of the
    % control volume to the system matrix
    l = k+1;
    A_e = Gamma_ew(:,:,l)\U_ew(:,:,l);
    %  eigenvalues/vectors of A, etc
    [ V,Lambda ] = eig( A_e );
%     if Schm == 1
%         S = V*sign(Lambda)/V;
%         Sigma = Gamma_ew(:,:,1)*S/Gamma_ew(:,:,1);
%     end
    
    %  Bernoulli coefficient matrices, etc.
    lambda = diag(Lambda);
    mu = dltx*lambda;
    diagvec = scBernoulli(mu);
    Bplus = V*diag(diagvec)/V;
    diagminvec = scBernoulli(-mu);
    Bmin = V*diag(diagminvec)/V;
%     if Schm == 1
%         Wplus_e = V*diag( [ scWeightf(mu1) scWeightf(mu2) scWeightf(mu3) ] )/V;
%     end
    %  matrix coefficients corresponding to homogeneous flux
    AW_e = Gamma_ew(:,:,l)*Bmin/dltx; AE_e = Gamma_ew(:,:,l)*Bplus/dltx;
    
    AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) = AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+1:NrParticles*k+NrParticles) + AW_e/urf;
    AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+(NrParticles+1):NrParticles*k+2*NrParticles) = AA(NrParticles*k+1:NrParticles*k+NrParticles,NrParticles*k+(NrParticles+1):NrParticles*k+2*NrParticles) - AE_e;
    
%     if Schm == 1
%         Q = 0.5*eye(NrParticles) - Gamma_ew(:,:,l)*Wplus_e/Gamma_ew(:,:,l);
%         BW_e = 0.5*Q*( eye(NrParticles) + Sigma ); BE_e = 0.5*Q*( eye(NrParticles) - Sigma );
%         BB(4*k+1:4*k+4,4*k+1:4*k+4) = BB(4*k+1:4*k+4,4*k+1:4*k+4) - BW_e;
%         BB(4*k+1:4*k+4,4*k+5:4*k+8) = BB(4*k+1:4*k+4,4*k+5:4*k+8) - BE_e;
%     end
    
    % under relaxation
    bb(NrParticles*k+1:NrParticles*k+NrParticles) = bb(NrParticles*k+1:NrParticles*k+NrParticles) + (1 - urf)/urf*AW_e*solutionVector;
    AW_w = AW_e;
    AE_w = AE_e;
%     if Schm == 1
%         BW_w = BW_e;
%         BE_w = BE_e;
%     end
    
end

AA(1:NrParticles,1:NrParticles) = eye(NrParticles);
AA(NrParticles*N-(NrParticles-1):NrParticles*N,NrParticles*N-(NrParticles-1):NrParticles*N) = eye(NrParticles);

% if Schm == 1
%     %  complete flux scheme
%     source = zeros(4*N,1);
%     for i = 1:N
%         source(4*i-3) = s1(i);
%         source(4*i-2) = s2(i);
%         source(4*i-1) = s3(i);
%         source(4*i) = s4(i);
%     end
%     %  computation right hand side
%     bb = bb + dltx*BB*source;
%     bb(1:4) = bnd_l;
%     bb(4*N-3:4*N) = bnd_r;
%     %  compute numerical solution
%     v = AA\bb;
%     for j = 1:N
%         phi1(j) = v(4*j-3); 
%         phi2(j) = v(4*j-2);
%         phi3(j) = v(4*j-1); 
%         phi4(j) = v(4*j);
%     end
% else
    %  exponential scheme
    %  computation right hand side
    for j=2:N-1
        bb(NrParticles*j-(NrParticles-1):NrParticles*j) = bb(NrParticles*j-(NrParticles-1):NrParticles*j) + dltx*Src(:,j) ;
    end
    bb(1:NrParticles) = bnd_l;
    bb(NrParticles*N-(NrParticles-1):NrParticles*N) = bnd_r;
    %  compute numerical solution
    v = AA\bb;
    for j = 1:NrParticles
        for i = 1:N
            MassFractionMatrix(i,j) = v(NrParticles*i- (NrParticles-j));
        end
    end
%end
