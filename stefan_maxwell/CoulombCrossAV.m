% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function y = CoulombCrossAV( Tij, Zi, Zj, particleList)

b0 = ImpactParameterEIAV( Zi, Zj, Tij);

%{
Calculate the Debye length
%}
elementary_charge=PhysConst.e;
fac = PhysConst.eps0 * PhysConst.k_B / elementary_charge^2;
N_Tratio = 0.0;
for i = 1:length(particleList)
	N_Tratio = N_Tratio + ( elementary_charge.*particleList{i}.charge)^2*particleList{i}.density/ particleList{i}.temperature;
end
debye_length=sqrt ( fac / N_Tratio );

coulomb_lambda=debye_length/b0;
coulomb_log=max([1,log(1+coulomb_lambda)]);

y = 6.0*pi*b0^2*coulomb_log;
end
