% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function y = DebyeLength( particleList )

fac = PhysConst.eps0 * PhysConst.k_B / PhysConst.e^2;
N_Tratio = 0.0;
for i = 1:length(particleList)
	N_Tratio = N_Tratio + ( PhysConst.e.*particleList{i}.charge)^2*particleList{i}.density/ particleList{i}.temperature;
end

y = sqrt ( fac / N_Tratio );
