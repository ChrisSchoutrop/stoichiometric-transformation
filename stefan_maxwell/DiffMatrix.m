% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function Damb = DiffMatrix(N, NrParticles, Y_ew, Prs, m_ew, Z_ew, rho_ew, n_ew, particleList)

% This function calculates the flux diffusion matrix
Damb = zeros(NrParticles, NrParticles, N-1);

% Boltzmaan constant
Kb = 1.3806503e-23; 

% collision integral
% Note that it is defined on the control volume interfaces
Omega11 = collisionInt(N-1, NrParticles, particleList);

% binary diffusion coefficient, see eq. (3.29) of Ger Janssen's thesis
mathcalD = zeros(NrParticles,NrParticles,N-1);
f_ij = 1.0;
for k = 1:N-1
    for i= 1:NrParticles
        for j = 1:NrParticles
            Mij = ( particleList(i).Mass * particleList(j).Mass )/(particleList(i).Mass + particleList(j).Mass);
            mathcalD(i,j,k) = 3/16*f_ij*Kb^2*particleList(i).Temperature*particleList(j).Temperature/(Prs*Mij*Omega11(i,j,k));
        end
    end
end

% fraction coefficients f_{i,j}, see eq.(4.9) of Kim's thesis
f = zeros(NrParticles,NrParticles,N-1);
for k = 1:N-1
    for i= 1:NrParticles
        for j = 1:NrParticles
            if j == i
                f(i,j,k) = 0;
            else
                f(i,j,k) = Z_ew(i,k)*Z_ew(j,k)/mathcalD(i,j,k);
            end
        end
    end
end

% fraction matrix F, see eq.(4.8) of Kim's thesis
F = zeros(NrParticles,NrParticles,N-1);
for k = 1:N-1
    for i= 1:NrParticles
        for j = 1:NrParticles
            if j == i
                F(i,j,k) = sum(f(i,:,k));
            else
                F(i,j,k) = -f(i,j,k);
            end
        end
    end
end

% after regularization F becomes F_tilde
F_tilde = zeros(NrParticles,NrParticles,N-1);
% the inverse of F_tilde
D_tilde = zeros(NrParticles,NrParticles,N-1);
% multicomponent diffusion matrix see eq.(4.6) in Kim's thesis
D = zeros(NrParticles,NrParticles,N-1);
for k = 1:N-1
    alpha = 1/max(max(mathcalD(:,:,k)));
    F_tilde(:,:,k) = F(:,:,k) + alpha*Y_ew(:,k)*Y_ew(:,k)';
    D_tilde(:,:,k) = inv(F_tilde(:,:,k));
    D(:,:,k) = D_tilde(:,:,k) - 1/alpha;
end

rvec = zeros(NrParticles, 1);
zvec = zeros(NrParticles, N-1);
for i= 1:NrParticles
    rvec(i) = particleList(i).Q/particleList(i).Mass;
    zvec(i,:) = n_ew(i,:)*particleList(i).Q;
end

Dz = zeros(NrParticles, N-1);
DzDz = zeros(NrParticles, NrParticles, N-1);
for k = 1:N-1
    Dz(:,k) = D(:,:,k)*zvec(:,k);
    DzDz(:,:,k) = Dz(:,k)*Dz(:,k)';
    Damb(:,:,k) = D(:,:,k) - DzDz(:,:,k)/(zvec(:,k)'*Dz(:,k));
end

%%%%%%%%%%%%%%% calculate M_tilde %%%%%%%%%%%%%%%%%%%%%%%
% sigma = zeros(N-1, 1);
% for i = 1:NrParticles
%     sigma = sigma + Y_ew(i, :)';
% end

% matrix in the relation of the gradient between mass and mole fractions,
% see eq.(4.18) of Kim's thesis
M_tilde = zeros(NrParticles,NrParticles,N-1); 
% for k = 1:N-1
%     for i = 1:NrParticles
%         for j = 1:NrParticles
%             if j == i
%                 M_tilde(i,j,k)  = m_ew(k)/particleList(i).Mass*(Y_ew(i,k) - Z_ew(i,k) + sigma(k));
%             else
%                 M_tilde(i,j,k) = m_ew(k)*(Y_ew(i,k)/particleList(i).Mass - Z_ew(i,k)/particleList(j).Mass);
%             end
%         end
%     end
% end

for k = 1:N-1
    M_tilde(:,:,k) = (diag(Z_ew(:,k)) + Z_ew(:,k)*(Y_ew(:,k) - Z_ew(:,k))'/sum(Z_ew(:,k)))*inv(diag(Y_ew(:,k)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for k = 1:N-1
    Damb(:,:,k) = Damb(:,:,k)*M_tilde(:,:,k);
end

% regularization, see eq.(5.20) of Kim's thesis
Dr = zeros(NrParticles, N-1);
for k = 1:N-1
    Dr(:,k) = D(:,:,k)*rvec;
end

uu = ones(NrParticles,NrParticles);
rr = rvec*rvec';

gamma = zeros(N-1,1);
for k = 1:N-1
    gamma(k) = 1.0e5*(rvec'*Dr(:,k))/((rvec'*rvec)^2);
    Damb(:,:,k) = Damb(:,:,k) + 1/alpha*uu + gamma(k)*rr;
end

%%%%%%%%%%%%%%%%%%%% calculate R %%%%%%%%%%%%%%%%%%%%
% see eq.(4.28) of kim's thesis
R = zeros(NrParticles,NrParticles,N-1);
for k = 1:N-1
    R(:,:,k) = diag(rho_ew(k)*Y_ew(:,k));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for k=1:N-1
    Damb(:,:,k) = R(:,:,k)*Damb(:,:,k);
end
