% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function y = LangevinCrossAV(mu, rel_pol, Z, T)

mean_vel_diff = sqrt(8*PhysConst.k_B*T/(pi*mu));
langevin = LangevinCross(mu,rel_pol,Z,mean_vel_diff);
y = (4/3)*langevin; 
end
