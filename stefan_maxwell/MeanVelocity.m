% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
%  calculates the mean thermal velocity of particles with temperature T
%  and mass M. A Maxwellian distribution is assumed. Units: SI.
%  Note: T and M can also be T_ij and m_ij !!!!

function y = MeanVelocity( T,  M)

y = sqrt((8.0*PhysConst.k_B/pi)*T/M);
