% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function y = collisionInt(particleList)

%This function calculates collision integral which will be
%used for calculating binary diffusion coefficient

NrParticles=length(particleList);

crossSection = zeros(NrParticles,NrParticles);
%crossSection is symmetric
for i = 1:NrParticles
    for j = 1:NrParticles
        if j<=i
            crossSection(i,j) = plAveragedCrossSectionFunc(particleList{i}, particleList{j}, particleList);
        end
    end
end
%Symmetrize crossSection
crossSection = (crossSection+crossSection') - eye(size(crossSection,1)).*diag(crossSection);

%Mean thermal velocity
%V_th is symmetric
V_th = zeros(NrParticles,NrParticles);
for i = 1:NrParticles
    for j=1:NrParticles
        if j<=i
            mass_i=particleList{i}.mass;
            mass_j=particleList{j}.mass;
            Tij = (mass_i * particleList{j}.temperature + mass_j * particleList{i}.temperature) / (mass_i + mass_j);
            Mij = (mass_i * mass_j) / (mass_i + mass_j);
            V_th(i,j) = MeanVelocity(Tij,  Mij);
        end
    end
end
%Symmetrize V_th
V_th = (V_th+V_th') - eye(size(V_th,1)).*diag(V_th);

y = 0.25*crossSection.*V_th;
end
