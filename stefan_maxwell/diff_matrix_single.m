% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function diff_matrix = diff_matrix_single(y, pressure, x, rho, densities, particleList)
%{
    Based on "DiffMatrix.m" from Lei Liu, slightly adapted to only
    calculate a single diffusion matrix, instead of in all gridpoints.
    
    For consistency the gridpoint handling is done elsewhere.
    
    INPUT: at a given gridpoint this needs
        y               Mass fractions
        pressure        Pressure [Pa]
        x               Mole fractions
        rho             sum(m_i*n_i)
        densities       number densities[m^-3]
        particleList    Array of structs defined in the inputfile
        containing things like mass, charge etc of a species.
    OUTPUT:
        Damb            Ambipolar diffusion matrix (E)
    References:
        Beks-Peerenboom, Kim. (2012). Modeling of magnetized expanding plasmas (Doctoral dissertation, PhD thesis, Eindhoven University of Technology, Eindhoven, July).
        Ramshaw, John. D. (1996). Simple Approximation for Thermal Diffusion in lonized Gas Mixtures.
        Janssen, Ger. M. (2000). Design of a general plasma simulation model, fundamental aspects and applications. Eindhoven University of Technology
%}
%Number of particles
NrParticles=length(y);
%{
    Binary diffusion coefficient matrix, see eq. (3.29) of Ger Janssen's thesis
    and the work of Ramshaw
%}
%Binary diffusion coefficient, see eq. (3.29) of Ger Janssen's thesis
mathcalD = zeros(NrParticles,NrParticles);
%Collision integral
Omega11 = collisionInt(particleList);
%mathcalD is also symmetric, so we only have to calculate half
for i= 1:NrParticles
    for j = 1:NrParticles
        if j<=i
            if strcmp(particleList{i}.label,'e[-1]')==false && strcmp(particleList{j}.label,'e[-1]')==false
                %If one of the particles involved is an electron,
                %then f_ij~=1. This is dictated by f(Z_i), where Z_i is the
                %charge number of species i defined as Z_i=abs(charge_i/charge_electron)
                f_ij=1;
            else
                Z_i=max([abs(particleList{i}.charge) abs(particleList{j}.charge)]);
                %The electron will always have charge 1, however the ion
                %can have more. Additionally f_i{electron}=f_{electron}i.
                switch Z_i
                    case 1
                        f_ij=1.97;
                    case 2
                        f_ij=2.33;
                    case 3
                        f_ij=2.53;
                    otherwise
                        %Ramshaw does not list the values for Z_i>3.
                        f_ij=1;
                        error('Correction factor for binary diffusion coefficient with highly charged ions not implemented.')
                end
                
            end
            mass_i=particleList{i}.mass;
            mass_j=particleList{j}.mass;
            Mij = ( mass_i*mass_j )./(mass_i + mass_j);
            mathcalD(i,j) = (3/16).*f_ij.*PhysConst.k_B^2.*particleList{i}.temperature.*particleList{j}.temperature./(pressure.*Mij.*Omega11(i,j));
        end
    end
end
%Symmetrize mathcalD
mathcalD = (mathcalD+mathcalD') - eye(size(mathcalD,1)).*diag(mathcalD);

%Friction coefficients f_{i,j}, see eq.(4.9) of Kim's thesis
%Symmetric
f = zeros(NrParticles,NrParticles);
z=zeros(NrParticles,1);
for i=1:NrParticles
    p_i=densities(i).*PhysConst.k_B.*particleList{i}.temperature;
    z(i)=p_i/pressure;
end

for i= 1:NrParticles
    for j = 1:NrParticles
        if j<=i
            if j == i
                f(i,j) = 0;
            else
                f(i,j) = z(i)*z(j)/mathcalD(i,j);
            end
        end
    end
end
%Symmetrize f
f = (f+f') - eye(size(f,1)).*diag(f);

%Friction matrix F, see eq.(4.8) of Kim's thesis
%Symmetric
F = zeros(NrParticles,NrParticles);
for i= 1:NrParticles
    for j = 1:NrParticles
        if j<=i
            if j == i
                F(i,j) = sum(f(i,:));
            else
                F(i,j) = -f(i,j);
            end
        end
    end
end
%Symmetrize F
F = (F+F') - eye(size(F,1)).*diag(F);

%{
    After regularization F becomes F_tilde
    The inverse of F_tilde is given by D_tilde
%}
F_tilde=regularize(F,y);
%F_tilde is symmetric positive definite
%{
F has an left-eigenvector of all ones with eigenvalue zero.
[V,L]=eig(F)
cond(F)
To remove this singularity, while maintaining the same solution for the
diffusion velocities we add a matrix alpha*y*y'.
y is not an eigenvector of F
[V,L]=eig(F_tilde)
cond(F_tilde)
%}
%{
    Scale the matrix F_tilde with the inverse row and column-sums to improve conditioning.
%}
Q=diag(1./sum(abs(F_tilde),2));
P=diag(1./sum(abs(Q*F_tilde),1));
D_tilde=P*inv(Q*F_tilde*P)*Q; %#ok<MINV>

%{
    The matrix D_tilde relates diffusion velocities to gradients of mole
    fractions
%}
r = zeros(NrParticles, 1);
c = zeros(NrParticles, 1);
for i= 1:NrParticles
    r(i) = densities(i)*PhysConst.e.*particleList{i}.charge;
    c(i) = particleList{i}.charge;
end

if all(c==0)==false
    D_amb=D_tilde-(D_tilde*(r*r')*D_tilde)/(r'*D_tilde*r);
    %beta=1;
    %D_amb_tilde=D_amb+beta*c*c';
    D_amb_tilde=regularize(D_amb,c);
    %D_amb_tilde=D_amb+beta*rvec*rvec';
    %{
    q'*x=sum(q_ix_i)=sum(q_i n_i)/n=0 by the quasineutral approximation.
    The elementary charge e has been divided out to scale to O(1).
    [V,L]=eig(D_amb) %This has z^T as left-eigenvector with eigenvalue 0
    cond(D_amb)
    [V,L]=eig(D_amb_tilde) %This is a regular matrix
    cond(D_amb_tilde)
    %}
else
    D_amb_tilde=D_tilde;
end

%cond(D_amb_tilde)
R = diag(rho*y);

%{
    Final output
Does this have the left-eigenvectors [Z_1,...,Z_m] and 1^T?
%}
m=zeros(NrParticles,1);
for i= 1:NrParticles
    m(i)=particleList{i}.mass;
end
%{
    M_tilde-matrix
    This relates the gradient in mass fractions to the gradient in mole
    fractions. d(mole_frac)/dx=M_tilde*d(mass_frac)/dx. eq.(4.18) of Kim's
    thesis.
%}
mu=x'*m./m;
M_tilde = diag(mu)*(eye(NrParticles)-y*mu'+ones(NrParticles)/sum(mu));


diff_matrix = R*D_amb_tilde*M_tilde;

% if any(eig(diff_matrix)<=0)
%     %Sanity check
%     %Damb should be positive definite; all eigenvalues >0.
%     error('Diffusion matrix is not positive definite!')
% end

return


