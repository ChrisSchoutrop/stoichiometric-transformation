% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
% Note that the averaged cross section is specie-dependent because in this
% function DEBYELENGTH density is used.

function y = plAveragedCrossSectionFunc( p1, p2, particleList)

% Effective temperature, reduced Mass, collisional diameter:
Tij = ( p1.mass *p2.temperature + p2.mass *p1.temperature) / (p1.mass + p2.mass);
Mij = ( p1.mass * p2.mass ) / (p1.mass + p2.mass);

Dij = p1.radius + p2.radius;
Zi = p1.charge;
Zj = p2.charge;

if ( (Zi == 0) && (Zj == 0) )
    % Interaction between two neutral species ? ->  hard sphere
    y = HardSphereCrossAV( Dij);
    
elseif ( (Zi ~= 0) && (Zj ~= 0) )
    % Interaction between two charged species ? ->  Coulomb.
    y = CoulombCrossAV( Tij, Zi, Zj, particleList);
else
    %{
    mixed case: neutral - charged interaction: Langevin
    defined such that alpha_p = rel_pol*a0^3 is equal to (Dij/2)^3.
    Lieberman, M. A., & Lichtenberg, A. J. (2005). Principles of plasma discharges and materials processing. John Wiley & Sons.
    p. 62
    %}
    
    rel_pol = ( (0.5*Dij)/PhysConst.a0 )^3;
    % Z is the charge of the charged particle.
    Z = Zi+Zj;
    y = LangevinCrossAV( Mij,  rel_pol, Z , Tij);
    
end
