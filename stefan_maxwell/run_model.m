% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
% This model is an ambipolar diffusion simulation of multicomponent mixture.
%
% NOTATIONS:
%            the meanings of most variables can be found near the first-time
%            appearing points
%            subscript 'ew': the variables with it defined on cvflux(interface of
%            control volume) points
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; format long e;

% if Schm == 1, CF scheme is applied, otherwise, exponential scheme
Schm = 0;

% relaxation factor(important when the flow velocity is zero)
urf = 1.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% constants %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Boltzmaan constant (m^2 Kg s^-2 K^-1)
Kb = 1.3806503e-23;

% elemenatray charge(C)
ElementaryCharge = 1.60217646e-19;

% mixture gas pressure (Pa)
Prs = 5000;

% flow velocity(Kg m^-2 s^-1)
rhov = 0.0;

% atomic mass unit (kg).
AMU	= 1.66053873e-27;

% initialize the preperties of the species 
% new ions can be added here. Don't forget to change the ions number variable NrIons
% NOTE that so far only H^+ can be added because of the implimentation of the initial values 
particleList(1).Name = 'electron';
particleList(1).Mass = 9.10938189e-31;
particleList(1).ChargeNr = -1; 
particleList(1).Temperature = 50000;
particleList(1).Radius = 0;
particleList(1).Q = -ElementaryCharge;

particleList(2).Name = 'hydrogen atom';
particleList(2).Mass = 1.008*AMU;
particleList(2).ChargeNr = 0;
particleList(2).Temperature = 8000;
particleList(2).Radius = 5.3e-11;
particleList(2).Q = 0;

particleList(3).Name = 'hydrogen ion';
particleList(3).Mass = 1.008*AMU;
particleList(3).ChargeNr = 1;
particleList(3).Temperature = 8000;
particleList(3).Radius = 1e-14;
particleList(3).Q = ElementaryCharge;

% particleList(4).Name = 'hydrogen ion';
% particleList(4).Mass = 1.008*AMU;
% particleList(4).ChargeNr = 1;
% particleList(4).Temperature = 8000;
% particleList(4).Radius = 1e-14;
% particleList(4).Q = ElementaryCharge;

% Number of ions
NrIons = 1;
% Number of species
NrParticles = size(particleList, 2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Grid layout
%       o---|---o---|---o---|---o---|---o---|---o---|---o---|---o
% where |: interfaces of control volumes(cv), o: nodal points

% length of the chamber
L = 0.002;
% number of grid points
N = 201;
% grid size
dltx = L/(N-1);
% coordinates of the nodal points
x = dltx*(0:N-1)';
% coordinates of the cv interfaces
x_ew = dltx*((0:N-2)' + 0.5);

% % reaction rate coefficient
% % The rate coefficient of the reaction is calculated by Arrhenius coefficient
% C = 6e-16;
% T_exp = 0.073;
% % energy of H
% E_H = 2.3*eV;
% % energy of H_2
% E_H_2 = 0;
% % activation energy of the reaction 2H_2 --> 2H + H_2
% E_a = 2*E_H - E_H_2;
% K = C*Tmp.^T_exp.*exp( - E_a./(Kb*Tmp) );

% initial values of Mass fractions
for i = 1:NrIons
    particleList(NrParticles - i + 1).MassFraction = 1e-3*ones(N,1);
end
particleList(1).MassFraction = NrIons*particleList(3).MassFraction*particleList(1).Mass./particleList(3).Mass;
particleList(2).MassFraction = 1 - NrIons*particleList(3).MassFraction - particleList(1).MassFraction;

% Dirichlet boundary condition
% the mass fractions of ions are assigned then that of electrons is calculated according to the 
% neutral constraint
for i = 1:NrIons
    particleList(NrParticles - i + 1).MassFraction(1) = 1e-2;
end
particleList(1).MassFraction(1) = NrIons*particleList(3).MassFraction(1)*particleList(1).Mass/particleList(3).Mass;
particleList(2).MassFraction(1) = 1 - NrIons*particleList(3).MassFraction(1) - particleList(1).MassFraction(1);

for i = 1:NrIons
    particleList(NrParticles - i + 1).MassFraction(N) = 1e-3;
end
particleList(1).MassFraction(N) = NrIons*particleList(3).MassFraction(N)*particleList(1).Mass/particleList(3).Mass;
particleList(2).MassFraction(N) = 1 - NrIons*particleList(3).MassFraction(N) - particleList(1).MassFraction(N);

Y_ew = zeros(NrParticles, N-1);
Z_ew = zeros(NrParticles, N-1);
n_ew = zeros(NrParticles, N-1);
Src = zeros(NrParticles, N);
bnd_l = zeros(NrParticles, 1);
bnd_r = zeros(NrParticles, 1);

% iteration starts...
for iter = 1:2000
  
    % solution of the previous iteration step
    particleList_old = particleList;
     
    % interpolate the Mass fractions to cv interfaces
    for i = 1:NrParticles
        Y_ew(i, :) = 0.5*(particleList(i).MassFraction(1:N-1)' + particleList(i).MassFraction(2:N)');
    end

    % m is defined in the relation between mole and mass fraction
    sum1 = zeros(N,1);
    for i = 1:NrParticles
        sum1 = sum1 + particleList(i).MassFraction/particleList(i).Mass;
    end
    m = 1./sum1; % eq.(11) of Kim's paper
    m_ew = 0.5*(m(1:N-1) + m(2:N));
    
    % mole fractions on nodal points and cv interfaces
    for i = 1:NrParticles
        particleList(i).MoleFraction = particleList(i).MassFraction.*m/particleList(i).Mass;
        Z_ew(i, :) = 0.5*(particleList(i).MoleFraction(1:N-1)' + particleList(i).MoleFraction(2:N)');
    end
     
    % mixture density on nodal points and cv interfaces
    sum2 = zeros(N,1);
    for i = 1:NrParticles
        sum2 = sum2 + particleList(i).MassFraction*particleList(i).Temperature/particleList(i).Mass;
    end
    rho = Prs./(Kb*sum2);
    rho_ew = 0.5*(rho(1:N-1) + rho(2:N));
    
    % densites
    for i = 1:NrParticles
        particleList(i).Density = rho.*particleList(i).MassFraction/particleList(i).Mass;
        % interpolate the densities to the cv interfaces
        n_ew(i, :) = 0.5*(particleList(i).Density(1:N-1)' + particleList(i).Density(2:N)');
    end

    % calculate the diffusion matrix
    Gamma_ew = DiffMatrix(N, NrParticles, Y_ew, Prs, m_ew, Z_ew, rho_ew, n_ew, particleList);
    
    % advection matrix
    U_ew = zeros(NrParticles,NrParticles,N-1);
    for k = 1:N-1
        U_ew(:,:,k) = rhov*eye(NrParticles);
    end
    
    % source terms
    for i = 1:NrParticles
        Src(i,:) = zeros(1, N);
    end
    
    % boundary condition
    for i = 1:NrParticles
        bnd_l(i) = particleList(i).MassFraction(1);
        bnd_r(i) = particleList(i).MassFraction(N);
    end
    
    % solve the system
    MassFractionMatrix = CFsolver(N,  NrParticles, dltx, particleList_old, Src, Gamma_ew, U_ew, bnd_l, bnd_r, urf, Schm);
   
    for i = 1:NrParticles
        particleList(i).MassFraction = MassFractionMatrix(:,i);
    end
    
    residual = norm(abs(particleList(1).MassFraction - particleList_old(1).MassFraction)/max(particleList_old(1).MassFraction), inf);
    fprintf('iter = %d\n',iter);
    fprintf('Residual = %e\n', residual);
    % iteration stop criterion
    if residual < 1e-12
        break;
    end
    
    
    plot(x, particleList(3).MassFraction)
    Mov(iter) = getframe;
end % end of iteration

% if Schm == 1
%     title('CF scheme');
% else
%     title('Exponential scheme');
% end

movie(Mov);
