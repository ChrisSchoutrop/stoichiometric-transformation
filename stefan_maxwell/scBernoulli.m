% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
function y = scBernoulli(z)
%function: BF:  z - > z/(exp(z)-1)
%input: z real value
%output: y real value

y = zeros(size(z));
smallentries = abs(z) <= 1e-4;
rest = not(smallentries);
y(smallentries) = 1 - (z(smallentries)/2).*( 1-z(smallentries)/6 );
y(rest) = z(rest) ./ (exp(z(rest))-1);
