% Copyright (C) 2019-2020 Eindhoven University of Technology.
%
% This code is free software, you can redistribute it and/or modify it under
% the terms of the GNU General Public License; either version 3.0 of the
% License, or (at your option) any later version. See LICENSE for details.
%
% This code is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
% Authors: Chris Schoutrop, Jan van Dijk and Jan ten Thije Boonkkamp
%          The Plasimo Team (https://plasimo.phys.tue.nl)
classdef tcf_system < handle
    %{
    Contains everything needed to solve the coupled differential equation:
    dphi/dt+d/dx(U*phi-E*dphi/dx)=s
    where U,E are matrices of size m x m
    phi,s vectors of size m x 1
    m is the number of species in the mixture
    %}
    
    properties (Constant)
        scheme='CF' %CF=Complete Flux, CD=Central Difference
    end
    
    properties
        tmin    %Initial time
        tmax    %Final time, if tmax is inf, then we solve the steady state problem
        dt      %Time step, irrelevant for steady state problem
        t       %Current time
        
        tcf_grid    %Grid with N gridvariables
        
        tol_phi         %Tolerance for the entire set of equations
        max_iter_phi;   %Maximum number of iterations for the building block (eta) equations
        
        continue_stepping=true
        
        m   %Number of species
        
        theta           %Parameter for the theta-integration scheme
        urf_phi=1;      %Underrelaxation parameter
        do_Newton=false;%Use Quasi-Newton iterations or Picard?
        
        input_struct
    end
    
    methods (Access=public)
        function obj=tcf_system(input_struct)
            %Constructor
            
            %Set up grid from input struct
            obj.tcf_grid=grid(input_struct);
            obj.continue_stepping=true;
            
            %Process other variables from input struct
            obj.tmin=input_struct.tmin;
            obj.tmax=input_struct.tmax;
            obj.dt=input_struct.dt;
            
            obj.tol_phi=input_struct.tol_phi;
            obj.max_iter_phi=input_struct.max_iter_phi;
            
            obj.t=obj.tmin;
            obj.m=length(input_struct.M);
            obj.theta=input_struct.theta;
            
            obj.urf_phi=input_struct.urf_phi;
            
            obj.input_struct=input_struct;
        end
        function obj=advancetimestep_Picard(obj)
            %{
            Solve when iterations are needed; i.e. when terms depend
            phi using Picard iterations for the source term.
            %}
            residual_phi=inf;
            update_size=inf;
            
            %Start with estimate of phi^{n+1}
            grid_new=NaN;
            
            iter_phi=0;
            
            while (residual_phi>obj.tol_phi ||  update_size>obj.tol_phi) && iter_phi<obj.max_iter_phi
                %{
                Continue iterating until:
                -residual is smaller than the tolerance, AND the update
                size is smaller than the tolerance.
                
                Stop if the amount of iterations has exceeded the maximum.
                %}
                
                %Start with estimate of phi^{n+1}
                if iter_phi==0
                    obj.tcf_grid.update_grid(obj.t);
                    grid_new=copy(obj.tcf_grid);
                    t_new=obj.t+obj.dt;
                    grid_new.update_grid(t_new);
                end
                
                %Solve phi
                B=blockmatrix();
                if strcmp(tcf_system.scheme,'CF')
                    if isinf(obj.tmax)
                        %Steady state solver
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,inf,false);
                    else
                        %Solves for 1 timestep
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,obj.dt,false);
                    end
                elseif strcmp(tcf_system.scheme,'CD')
                    B.assemble_phi_CD(obj.tcf_grid,grid_new,obj.theta,obj.dt);
                end
                phi_transformed_previous_iteration=grid_new.get_transformed_phi();
                
                if isempty(obj.input_struct.known_transformed)
                    %Solve the full system
                    B.solve_with_guess_Picard(phi_transformed_previous_iteration);
                else
                    %Solve the system where part of the transformed
                    %variables are already known
                    B.solve_with_guess_Picard_reduced(phi_transformed_previous_iteration,obj.input_struct.known_transformed);
                end
                
                if obj.urf_phi~=1
                    %
                    B.x=(1-obj.urf_phi).*phi_transformed_previous_iteration+obj.urf_phi.*B.x;
                end
                grid_new.set_transformed_phi(B.x);
                
                %{
                    Estimate residual with the new phi estimate
                    "How well do does the new phi fit with the new discrete
                    system?"
                %}
                grid_new.update_grid(t_new);
                if strcmp(tcf_system.scheme,'CF')
                    if isinf(obj.tmax)
                        %Steady state solver
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,inf,false);
                    else
                        %Solves for 1 timestep
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,obj.dt,false);
                    end
                elseif strcmp(tcf_system.scheme,'CD')
                    B.assemble_phi_CD(obj.tcf_grid,grid_new,obj.theta,obj.dt);
                end
                phi_new=grid_new.get_transformed_phi();
                update_size=norm(phi_new-phi_transformed_previous_iteration,2)./norm(phi_new,2);
                r=B.determine_relative_residual(B.x);
                residual_phi=r;
                iter_phi=iter_phi+1;
                disp(['iter_phi: ',num2str(iter_phi,16),' residual_phi: ',num2str(residual_phi,16),' update_size ',num2str(update_size,16)])
                try
                    %Update the diagnostic info if a previous iteration has
                    %been performed
                    obj.input_struct.phi.solve_time=obj.input_struct.phi.solve_time+B.solve_time;
                    obj.input_struct.phi.cond_unscaled=max(B.cond_unscaled,obj.input_struct.phi.cond_unscaled);
                    obj.input_struct.phi.cond_scaled=max(B.cond_scaled,obj.input_struct.phi.cond_scaled);
                    obj.input_struct.phi.residual=max(B.residual,obj.input_struct.phi.residual);
                    obj.input_struct.phi.error=min(residual_phi,obj.input_struct.phi.error);
                catch
                    %If this is the first time we get to this point, assign
                    %the diagnostic info to the struct
                    obj.input_struct.phi.solve_time=B.solve_time;
                    obj.input_struct.phi.cond_unscaled=B.cond_unscaled;
                    obj.input_struct.phi.cond_scaled=B.cond_scaled;
                    obj.input_struct.phi.residual=B.residual;
                    obj.input_struct.phi.error=residual_phi;
                end
                
                obj.input_struct.phi
                
            end
            disp(['t=',num2str(obj.tcf_grid.t+obj.dt)]);
            if residual_phi>obj.tol_phi
                warning('Error criterion was not reached!')
                disp(['Final residual reached was: ',num2str(residual_phi)])
            end

            if isinf(obj.tmax)
                %If a consistent solution is achieved via Picard iteration,
                %the steady state solver is finished.
                obj.continue_stepping=false;
            end
            %{
            %(old) Alternative method of obtaining steady state is via evolving
            %in time until dphi/dt~0.
            %Calculate relative change in this timestep
            %If dphi/dt is small enough, steady state is achieved.            
            if isinf(obj.tmax)
                phi_old=obj.tcf_grid.get_transformed_phi();
                phi_new=grid_new.get_transformed_phi();
                dphi_dt=(phi_new-phi_old)/obj.dt;
                dphi_dt=norm(dphi_dt,inf);
                %TODO: find a dimensionless criterion for this
                disp(['Estimated dphi/dt: ',num2str(norm(dphi_dt))])
                if dphi_dt<=obj.tol_phi || dphi_dt<=1e-10 || isfinite(dphi_dt)==false
                    obj.continue_stepping=false;
                end
            end
            %}
            obj.input_struct.phi.final_residual=residual_phi;
            obj.input_struct.phi.final_update_size=update_size;
            obj.input_struct.phi.iterations=iter_phi;
            obj.tcf_grid=copy(grid_new);
            obj.t=obj.tcf_grid.t;
        end
        function obj=advancetimestep_Newton(obj)
            %{
            Solve when iterations are needed; i.e. when terms depend
            phi using Newton iterations for the source term
            %}
            error_phi=inf;
            
            %Start with estimate of phi^{n+1}
            grid_new=NaN;
            
            iter_phi=0;
            update=[];
            update_size=inf;
            while (error_phi>obj.tol_phi ||  update_size>obj.tol_phi) && iter_phi<obj.max_iter_phi
                
                %Start with estimate of phi^{n+1}
                if iter_phi==0
                    obj.tcf_grid.update_grid(obj.t);
                    grid_new=copy(obj.tcf_grid);
                    t_new=obj.t+obj.dt;
                    grid_new.update_grid(t_new);
                end
                
                %Solve phi
                B=blockmatrix();
                if strcmp(tcf_system.scheme,'CF')
                    if isinf(obj.tmax)
                        %Steady state solver
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,inf,true);
                    else
                        %Solves for 1 timestep
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,obj.dt,true);
                    end
                elseif strcmp(tcf_system.scheme,'CD')
                    B.assemble_phi_CD(obj.tcf_grid,grid_new,obj.theta,obj.dt);
                end
                phi_transformed_previous_iteration=grid_new.get_transformed_phi();
                if isempty(obj.input_struct.known_transformed)
                    %Solve the full system
                    B.solve_with_guess_newton(phi_transformed_previous_iteration);
                else
                    %Solve the system where part of the transformed
                    %variables are already known
                    if isempty(update)==false
                        B.solve_with_guess_newton_reduced(-update,obj.input_struct.known_transformed);
                    else
                        B.solve_with_guess_newton_reduced(zeros(size(phi_transformed_previous_iteration)),obj.input_struct.known_transformed);
                    end
                    
                end
                
                %Apply the update computed by the Newton iteration
                update=-B.x;
                if isnan(obj.urf_phi)
                    %With automatic underrelaxation
                    error_phi=norm(B.r)./norm(B.b);
                    urf=1-min([0.75,error_phi.^1]);
                    disp(['urf: ',num2str(urf)])
                    phi_new=phi_transformed_previous_iteration+urf.*update;
                elseif obj.urf_phi~=1
                    %With underrelaxation
                    phi_new=phi_transformed_previous_iteration+obj.urf_phi.*update;
                else
                    %Without underrelaxation
                    phi_new=phi_transformed_previous_iteration+update;
                end
                
                grid_new.set_transformed_phi(phi_new);
                grid_new.update_grid(t_new);
                
                if strcmp(tcf_system.scheme,'CF')
                    if isinf(obj.tmax)
                        %Steady state solver
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,inf);
                    else
                        %Solves for 1 timestep
                        B.assemble_phi(obj.tcf_grid,grid_new,obj.theta,obj.dt);
                    end
                elseif strcmp(tcf_system.scheme,'CD')
                    B.assemble_phi_CD(obj.tcf_grid,grid_new,obj.theta,obj.dt);
                end
                %{
                Estimate residual with the new phi estimate
                "How well do does the new phi fit with the new discrete system?"
                Calculate the error to see if we've converged
                Technically this is the residual of the *previous* Newton
                iteration, but this will provide some safety margin.
                %}
                error_phi=norm(B.r)./norm(phi_new);
                update_size=norm(update)./norm(phi_new);
                iter_phi=iter_phi+1;
                disp(['iter_phi: ',num2str(iter_phi,16),' error_phi: ',num2str(error_phi,16)])
                
                %Save the diagnostic info
                try
                    obj.input_struct.phi.solve_time=B.solve_time+obj.input_struct.phi.solve_time;
                    obj.input_struct.phi.cond_unscaled=max(B.cond_unscaled,obj.input_struct.phi.cond_unscaled);
                    obj.input_struct.phi.cond_scaled=max(B.cond_scaled,obj.input_struct.phi.cond_scaled);
                    obj.input_struct.phi.residual=max(B.residual,obj.input_struct.phi.residual);
                    obj.input_struct.phi.error=min(error_phi,obj.input_struct.phi.error);
                catch
                    obj.input_struct.phi.solve_time=B.solve_time;
                    obj.input_struct.phi.cond_unscaled=B.cond_unscaled;
                    obj.input_struct.phi.cond_scaled=B.cond_scaled;
                    obj.input_struct.phi.residual=B.residual;
                    obj.input_struct.phi.error=error_phi;
                end
                
            end %while error_phi>obj.tol_phi && iter_phi<obj.max_iter_phi
            
            disp(['t=',num2str(obj.tcf_grid.t+obj.dt)]);
            if error_phi>obj.tol_phi
                warning('Error criterion was not reached!')
                disp(['Final tolerance reached was: ',num2str(error_phi)])
            end
            obj.input_struct.phi.final_tolerance=error_phi;
            try
                obj.input_struct.phi.iterations=obj.input_struct.phi.iterations+iter_phi;
            catch
                obj.input_struct.phi.iterations=iter_phi;
            end
            %{
            Calculate relative change in this timestep
            If dphi/dt is small enough, steady state is achieved.
            %}
            if isinf(obj.tmax)
                obj.continue_stepping=false;
            end
            %{
            %(old) Alternative method of obtaining steady state is via evolving
            %in time until dphi/dt~0.
            if isinf(obj.tmax)
                phi_old=obj.tcf_grid.get_transformed_phi();
                phi_new=grid_new.get_transformed_phi();
                dphi_dt=(phi_new-phi_old)/obj.dt;
                dphi_dt=norm(dphi_dt,inf);
                %TODO: find a dimensionless criterion for this
                disp(['Estimated dphi/dt: ',num2str(norm(dphi_dt))])
                if dphi_dt<=obj.tol_phi || dphi_dt<=1e-10 || isfinite(dphi_dt)==false
                    obj.continue_stepping=false;
                end
            end
            %}
            
            %Complete the iteration
            obj.tcf_grid=copy(grid_new);
            obj.t=obj.tcf_grid.t;
        end
    end
    
end

